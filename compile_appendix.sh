#!/bin/bash

set -xe

# then, the appendix
pandoc appendix.md -o appendix.tex --number-sections -H options.sty --template my_appendix.latex

sed -i 's/begin{figure}/begin{figure}\[H\]/g' appendix.tex

pdflatex appendix.tex
pdflatex appendix.tex
pdflatex appendix.tex

gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 \
   -dPDFSETTINGS=/prepress -dNOPAUSE -dQUIET -dBATCH \
   -sOutputFile=appendix_small.pdf appendix.pdf

mv appendix_small.pdf appendix.pdf
