set -xe

# first, the report
pandoc abstract.md -o abstract.tex --number-sections -H options.sty --template template_abstract.latex

sed -i 's/begin{figure}/begin{figure}\[H\]/g' abstract.tex

pdflatex abstract.tex
pdflatex abstract.tex
pdflatex abstract.tex

gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 \
   -dPDFSETTINGS=/prepress -dNOPAUSE -dQUIET -dBATCH \
   -sOutputFile=small_abstract.pdf abstract.pdf

mv small_abstract.pdf abstract.pdf
