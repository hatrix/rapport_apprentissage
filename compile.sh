set -xe

# first, the report
pandoc report.md -o report.tex --number-sections -H options.sty --template my.latex

sed -i 's/begin{figure}/begin{figure}\[H\]/g' report.tex

pdflatex report.tex
makeglossaries report
pdflatex report.tex
pdflatex report.tex

gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 \
   -dPDFSETTINGS=/prepress -dNOPAUSE -dQUIET -dBATCH \
   -sOutputFile=small_report.pdf report.pdf

mv small_report.pdf report.pdf
