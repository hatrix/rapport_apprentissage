[//]: # (Company presentation)
# History and operating sector

Scaleway, created in 1999 under the name _Online_, is a major actor in the
French and European cloud sector. Operating as part of the Iliad group (Free,
Free Mobile amongst others), Scaleway currently counts more than 200 employees
while looking to acquire even more talents; talents that are not so easy to
attract, the IT world being highly competitive.  

Being in the cloud sector, Scaleway owns more than 150,000 servers located in
its French datacenters. Currently at the number of 4, those datacenters
provide different resources for different expectations, ranging from high
security in a bunker shelter to hyper-scaling space. Sales regarding datacenters
are handled by the brand _Scaleway Datacenters_, whereas the dedicated servers
are handled by _Online by Scaleway_ and the cloud instances by _Scaleway_
itself.

With more than 60,000 active clients and 700 major accounts, Scaleway had a
turnover of 70 million euros in 2017. The major part of it, 60%, coming from
international clients spread across 150 countries.
Achieving top 10 cloud provider in Europe and top 3 in France, Scaleway sees 
itself competing with companies such as DigitalOcean or OVH.

Working premises include a house in the old city center of Lille and a building
in Paris 8e. The decoration and environment are made to make the employees
feel at home, thus the name of the premises "La Maison". Ranging from free
drinks and coffee to dishes prepped by a chef and even flexible hours,
everything is made to satisfy the personnel.

[//]: # (Prez compute)
# Compute Team

The team under which I operate, _Compute_, is one of the oldest in the history
of Scaleway. Being at the core of the company, its role is to develop and
maintain all the infrastructure related to instances. Including APIs, workers,
system administration and operational tasks, the tasks are extremely varied.  
Currently staffed at around a dozen employees, the team is split into two
_tracks_:

  * Instance Hardware Lifecycle (IHL): mainly handles operational tasks and 
  hypervisors software deployments
  * Instance Control Plane (ICP): handles development on APIs and workers.

My training supervisor, Thibault Billon, operates under the _ICP_ track, just as
myself. Having worked before for highly technological companies and for more
than 3 years now at Scaleway, Thibault possesses solid knowledge regarding
Scaleway's infrastructure.

[//]: # (Actual state of affaires)
# Infrastructure 

Upon my arrival, Scaleway had already initiated a _pivot_, recruiting more than
a hundred employees in a short period of time. Aimed to develop more products
and Scaleway itself, this pivot changed the way the _Compute_ team worked.
Work done by the team then was not so different from now: mainly consisting of
developing features for itself and other products while maintaining the
infrastructure needed for it.  

Said infrastructure was not automatically made redundant upon new software
development. Some well-needed features, particularly on the _api-marketplace_
and _api-compute_ were also absent. Those APIs are responsible respectively for
the display of current server images usable to clients and for operations on
servers such as creation, deletion, edits, etc.


[//]: # (Work + required skills asked by HR)
# Tasks and skills at the beginning

Being an apprentice, requirements for my recruitment were not as tough as for
everyone else, the goal indeed being to learn. Those conditions, despite being
more lenient, were still significant.  
Apart from sending my curriculum vitæ and a cover letter, I had to take four
distinct interviews:

* with HR, to get to know my motivations company-wise
* one technical, regarding REST APIs in Python
* another one technical, to assess architecture skills and reflection
* a one hour chat with one member of the team to check social skills

Main skills needed for those were Python, Bash and Linux as well as critical
thinking and knowledge about best practices.

First task assigned was about building server images for the Scaleway servers.
Those images are based on Docker and made me learn a great deal about it and
Linux systems in general.

[//]: # (Breakdown project -> graph)
# Breakdown

As part of a team composed of _devops'_, I did not only have one mission. But
rather several, sometimes even per week. Ranging from Python and Bash
development to system administration, those tasks often need to be given some
thoughts before being worked on. The complexity of Scaleway's technical stack
needs some time for new employees to be understood.

\begin{ganttchart}[
  x unit=.75cm
]{1}{14}
  \gantttitle{Agile Sprint on 2 weeks}{14} \\

  \ganttbar[name=saturday1-top,bar/.style={fill=none, draw=none}]{}{6}{6}
  \ganttbar[name=sunday1-top,bar/.style={fill=none, draw=none}]{}{7}{7}
  \ganttbar[name=saturday2-top,bar/.style={fill=none, draw=none}]{}{13}{13}
  \ganttbar[name=sunday2-top,bar/.style={fill=none, draw=none}]{}{14}{14}
  \gantttitlelist{1,...,14}{1} \\

  \ganttbar{Standups}{1}{1}
  \ganttbar{}{2}{2}
  \ganttbar{}{3}{3}
  \ganttbar{}{4}{4}
  \ganttbar{}{5}{5}
  \ganttbar{}{8}{8}
  \ganttbar{}{9}{9}
  \ganttbar{}{10}{10}
  \ganttbar{}{11}{11}
  \ganttbar{}{12}{12} \\

  \ganttbar{Sprint Planning}{1}{1} \\
  \ganttbar{Sprint Closing}{12}{12}

  \ganttbar[name=saturday1-bottom,bar/.style={fill=none, draw=none}]{}{6}{6}
  \ganttbar[name=sunday1-bottom,bar/.style={fill=none, draw=none}]{}{7}{7}
  \ganttbar[name=saturday2-bottom,bar/.style={fill=none, draw=none}]{}{13}{13}
  \ganttbar[name=sunday2-bottom,bar/.style={fill=none, draw=none}]{}{14}{14}

  \begin{scope}
    \draw [opacity=0.2,line width=12] (saturday1-top) -- ($(saturday1-bottom)+(0,-11pt)$);
    \draw [opacity=0.2,line width=12] (saturday2-top) -- ($(saturday2-bottom)+(0,-11pt)$);
    \draw [opacity=0.2,line width=12] (sunday1-top) -- ($(sunday1-bottom)+(0,-11pt)$);
    \draw [opacity=0.2,line width=12] (sunday2-top) -- ($(sunday2-bottom)+(0,-11pt)$);
  \end{scope}
\end{ganttchart}

To be organized correctly, the Agile method is used. This method imposes daily
short meetings to keep up to date on what others are doing. Every two weeks,
the team reflects on what has been done, what needs to be done, and if the
estimations were correct. While also counting the number of hours needed to
handle production problems (RUN), the team assesses its productivity and
ability to predict workloads for future tasks.

[//]: # (Working conditions + relationship with team members)
# Working conditions

Working conditions at Scaleway are one of the key advantages being highlighted
upon application. Each employee benefits a welcome package on arrival, composed
of a sweat-shirt, t-shirt and a mug.

Inside the premises, no desks are attributed, everyone can sit where they
please. This method known as _flex-office_ results in teams sometimes being
together, sometimes not, depending on each individual's mood. Thus, productivity
is increased, being able to sit with the more suitable people when needed.

Slack is our communication tool by default. It is used even if two people might
be in the same room. This comes from the habit to use it, with members of other
teams, the whole company or even people working in other premises or at home.

[//]: # (Acquired skills)
# Acquired skills throughout the apprenticeship

The first few months at Scaleway were hard. I did not possess to knowledge back
then to understand the complexity and technicality of Scaleway's
infrastructure.  
Not only the technical parts, but also the human ones. Scaleway having just
operated an important pivot and having a _startup ambiance_, it was hard to
accustom at first.

Change becoming frequent and developments moving forward, things nonetheless
got better. Through this apprenticeship, I discovered areas I was not familiar
with, despite thinking so. New skills were also learned on some programs I did
not know even existed before. The non-exhaustive list below gives some of the
skills I had the opportunity to develop:

* Python: Celery, Flask, SQLAlchemy
* PostgreSQL: usage in an high-availability context
* Bash
* Agile management
* Organizational matters through Jira, Confluence
* Git: working with multiple branches and teams with modifications everyday
* Information systems architecture

[//]: # (Critics of work -> highlights and negative points)

# Overview

Despite being hard at first, my apprenticeship at Scaleway was extremely
beneficial. Through this journey, I had the pleasure to learn amongst 
enthusiastic people, eager to teach me skills I lacked.

In a general manner, tasks have been done on time. Reviewed every time, the
quality is also correct, ensuring scalability and high-availability.  
Some of them could nonetheless have been done better, given time, that 
management unfortunately did not have. Some areas of Scaleway's infrastructure
also ought to be worked on, but are not considered a priority. My wish would be
that those part will be worked on after I leave, and not rendered obsolete
because of lack of interest.

This apprenticeship then, in the end, proved to be valuable. It made me meet 
incredible people and allowed be to work on amazing projects.

\label{End}
