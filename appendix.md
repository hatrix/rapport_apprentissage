\setlength{\abovecaptionskip}{0pt}

\label{Appendix}

\appendix
\rhead{Page \thepage\ sur \getpagerefnumber{End}}

# Pipeline de création d'instance

Les équipes front et design travaillent ensemble pour offrir aux utilisateurs
la meilleure expérience possible. Le pipeline de création d'instances se
retrouve ainsi simplifié et intuitif

![Dashboard Scaleway](./images/dashboard.png)

![Interface de gestion des instances](./images/pipe_1.png)

![Sélection de l'image serveur](./images/pipe_2.png)

![Sélection de la zone et du serveur](./images/pipe_4.png)

![Résumé et tarification des options sélectionnées](./images/pipe_5.png)

![Création de l'instance](./images/pipe_6.png)

Une fois créée, l'instance peut encore être modifiée dans une certaine limite. 
Son nom, ses volumes, ses IPs, son mode de boot ou encore les informations
cloud-init peuvent entre autres être modifiées. Seules la gamme et l'image ne
peuvent changer.

\newpage

# worker-transfers

\begin{figure}
   \centering
      \includegraphics[angle=90,height=20cm]{./images/worker-transfers.png}
      \caption{Proposition d'architecture pour le transfert d'images}
\end{figure}

\newpage

# Datacenters

Scaleway via sa marque _Scaleway Datacenter_ propose actuellement 4 datacenters
à Paris : DC2 et DC3 à Vitry, DC4 dans Paris 15e et DC5 à Saint-Ouen l'Aumône.

La visite d'au minimum un de nos datacenter est obligatoire pour les nouveaux
arrivants. Cela leur permet de prendre conscience de l'infrastructure
matérielle derrière les services que nous proposons aux clients.

## DC2

DC2 est actuellement le datacenter le plus de Scaleway en fonctionnement.
Regroupant les activités cloud de Scaleway et B2B, celui-ci est aujourd'hui 
complet.

![Entrée de DC2 à Vitry-Sur-Seine](./images/dc2_1.jpg)

\newpage
### Coupure fibre

Début 2019, un incident impliquant une pelleteuse lors de travaux touche DC2.[^1]

![Déclaration de l'incident](./images/tweet_pelleteuse.png)

![Fibre sectionnée](./images/fibre.jpg)

[^1]: https://twitter.com/Scaleway_fr/status/1093866327809445889

## DC3

DC3, la « Rolls-Royce » des datacenters, étonne de par sa sécurité et sa
technologie.[^3] Tout comme DC2, il accueille les activités de Scaleway mais
aussi du B2B.

![PC de sécurité de DC3](./images/dc3_1.jpg)

![Couloir sécurisé d'une salle de DC3](./images/dc3_2.jpg)

[^3]:https://lafibre.info/scaleway/dc3-iliad/

\newpage

## DC4

Situé dans un bunker à 26 mètres sous terre, DC4 offre une protection de taille
pour les clients nécessiteux. À l'origine, cet abri constitue un abri de 
défense passive dans les années 30, il est ensuite acquis et transformé par
Online.[^8]

Actuellement, DC4 héberge C14, l'offre cold storage de Scaleway. Ce datacenter
a été pensé pour héberger des données sensibles ainsi que le gouvernement,
répondant aux besoins du cloud souverain.[^9]

![Baies C14 dans DC4](./images/dc4_1.jpg)

Équipé de détecteurs sismiques, il n'est pas possible d'y accéder par
l'extérieur.

[^8]: https://blog.scaleway.com/2016/c14-story-part-1-meet-our-nuclear-fallout-shelter/
[^9]: https://www.programmez.com/actualites/scaleway-le-cloud-btb-diliad-devoile-sa-strategie-en-france-et-linternational-lors-de-son-premier-29061

\newpage
## DC5

Dernier datacenter en date, DC5[^2] opère via un refroidissement adiabatique.
Cette technologie permet d'économiser en électricité. Elle se repose en effet
sur l'échange d'humidité entre l'air chaud et l'air froid.

![Arnaud de Bermingham, CEO de Scaleway, et le refroidissement adiabatique](./images/dc5_1.jpg)

![SAS de sécurité véhicule DC5](./images/dc5_2.jpg)

[^2]: https://lafibre.info/scaleway/dc5/


### Incendie à proximité

Le 2 juin 2019, un incendie se déclare dans le bâtiment voisin de DC5.[^4] Cet
incendie a obligé le datacenter à couper ses groupes électrogène, ne
fonctionnant alors que sur arrivée EDF et batteries en cas de problème.
L'incendie a été suivi en temps réel par nos équipes qui ont ouvert l'accès
aux pompiers.[^5]

![Caméra de sécurité sur l'incendie](./images/dc5_3.png)

![Voie pompiers utilisée à DC5](./images/dc5_4.jpg)

[^4]: https://lafibre.info/scaleway/incendie-saint-ouen-laumone/
[^5]: https://twitter.com/a_bermingham/status/1135123239557091329

\newpage

# Tutoriels

De manière régulière, des tutoriels sont mis en ligne sur le site de Scaleway.[^6]
Ces tutoriels, permettent au client de prendre en main plus facilement nos
instances.

![Tutorial S3](./images/tuto_s3.png)

Complémentaires aux _instantapps_, ces tutoriels sont destinés aux clients
souhaitant configurer eux-mêmes leurs projets sans nécessairement avoir toutes
les bases requises


[^6]: https://www.scaleway.com/en/docs/

\newpage

# Workshops internes

Toutes les semaines, des ateliers et démonstrations sont réalisées. Ces séances
permettent aux collaborateurs de prendre connaissance des réalisations des 
autres équipes.

![Workshop IOT](./images/iot_workshop.jpg)

\newpage

# Scaleday - 20 ans d'Online

Pour fêter ses 20 ans, Scaleway décide d'organiser un après-midi de
conférences.[^7] Composé de deux keynotes, de conférences et d'une soirée, le
Scaleday a pour vocation de faire se rencontrer les clients et l'entreprise.  
Lors du Scaleday, ce sont plus de 90 annonces qui se suivent, sur différents
sujets :

* nouvelles fonctionnalités produits
* nouveaux produits
* nouveaux programmes

Situé dans les locaux de DC4, l'événement offre suffisamment de place pour
accueillir les 1400 personnes attendues.

![Keynote de fermeture](./images/scaleday_1.jpg)

L'ensemble des conférences peut-être retrouvé sur le site slideshare.[^10]

[^10]: https://fr.slideshare.net/scaleway

\newpage
Pour marquer le coup, une soirée est organisée pour les employés mais aussi et
surtout les clients. Gardant le même thème lumineux que les keynotes,
l'ambiance y est toutefois plus festive : bar à nourriture à volonté, DJ set,
combats de sabres lasers, paintball et stands photo.

![Soirée Scaleday](./images/scaleday_2.jpg)

[^7]: https://lafibre.info/scaleway/scaleday/

\newpage

# Journée Breakout

Pour fêter ses nouveaux produits et renforcer la cohésion d'équipe, une journée
_off_ est organisée le 21 mars 2019. Cette journée se déroule dans les locaux
de Lille : l'objectif est simplement de se faire plaisir avec diverses activités,
nourritures et boissons.

![Collaborateur en mauvaise position sur un taureau mécanique](./images/breakout_1.jpg)

![Food trucks et autres traiteurs étaient présents](./images/breakout_3.jpg)

\newpage

# Locaux

Scaleway propose à ses employés des locaux attractifs. Ouverts 24/24h et
agréables, ils permettent une meilleure cohésion d'équipe.  
Des événements y sont régulièrement organisés : barbecues, soirées jeux de 
société, apéritifs, etc.

## La Maison Paris v1

Située dans un hôtel particulier dans le 16e arrondissement, la Maison v1 
dénote de par son cadre et sa décoration.

![Vue sur l'arc de triomphe depuis la terrasse](./images/lamaison_1.jpg)

![Afterwork dans le patio](./images/lamaison_2.jpg)

![Salle des jeux](./images/lamaison_3.jpg)

## La Maison Paris v2

Courant avril 2019, Scaleway déménage dans le 8e arrondissement. Ces locaux,
plus spacieux, permettent d'avoir un effectif plus conséquent tout en gardant
l'esprit originel de décoration.

![Salle Jungle parisienne](./images/lamaison2.jpg)

## La Maison Lille

Un nombre croissant de collaborateurs travaillent à Lille. Situés dans le vieux
Lille, les locaux ont eu aussi le droit à une décoration particulière. Le style
du bâtiment est toutefois gardé, le moderne et l'ancien cohabitent.

![Salle Jungle lilloise](./images/lamaison_lille_2.jpg)


\label{End}
