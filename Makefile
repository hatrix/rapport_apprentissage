SHELL=/bin/bash

all: memoire appendix abstract count clean

memoire:
	./compile.sh

appendix:
	./compile_appendix.sh

abstract:
	./compile_abstract.sh

clean:
	rm -f *.aux *.glg *.glo *.gls *.ist *.lof
	rm -f *.log report.tex appendix.tex abstract.tex
	rm -f *.out *.toc
	rm -f *.glsdefs
	rm -f *.idx *.ilg *.ind

correct:
	hunspell -d fr_FR -t report.md
	hunspell -d fr_FR -t appendix.md
	hunspell -d en_US -t abstract.md

count:
	texcount report.tex
