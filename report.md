﻿# Remerciements

Je tiens tout d'abord à remercier EPITA\index{EPITA} et Scaleway au sens large, qui m'ont
permis de grandement faire évoluer mes compétences au fil des années.

La réalisation de ce rapport a été possible grâce à l'aide de plusieurs
personnes, issues aussi bien du cadre professionnel que personnel. Je souhaite
ainsi leur témoigner toute ma reconnaissance pour m'y avoir accompagné.

J'aimerais exprimer toute ma gratitude devant la bienfaisance et la patience
dont ont fait preuve certaines personnes lors des débuts de ma scolarité et
pendant celle-ci. C'est en grande partie grâce à eux que j'ai pu arriver où
j'en suis actuellement. C'est ainsi que je remercie, sans ordre particulier : 
Adrien Schildknecht, Antoine Lecubin, Paul Hervot, Rémi Audebert, Antoine
Pietri, Lili Guillemaud, Nahim El Atmani, Lionel Auroux, Dorian Hernandez,
Matthias Perrod.

Je souhaite ensuite exprimer ma reconnaissance à Kévin Deldycke, ami, puis
collègue, qui a cru en moi et qui a su faire le nécessaire pour me former et
m'intégrer dans Scaleway. Grâce à lui et à mon équipe, j'ai pu acquérir de
nombreuses connaissances aussi bien techniques que managériales en peu de
temps, en plus de vivre une expérience humaine hors du commun.  

J'adresse également mes remerciements à mes collègues et amis Hugues Morisset
et Thibault Billon, pour m'avoir particulièrement suivi et aidé durant les
moments les plus délicats. Leur support moral et technique a été d'une aide
précieuse bien plus d'une fois.

Je tiens enfin à remercier tous mes amis, d'EPITA\index{EPITA} et d'ailleurs, qui ont su me
supporter et m'apporter leur soutien.

\newpage

# Résumé de la mission

Pour une durée d'un an et demi, cet apprentissage prend place chez Scaleway,
acteur emblématique du _cloud_ en France et en Europe. Entreprise française et
filiale du groupe Iliad, Scaleway apporte son lot de défis techniques à chaque
employé.  
Ayant effectué un pivot stratégique fin 2017, Scaleway passe de 50
collaborateurs à plus de 250 aujourd'hui. Ce changement d'envergure engendre de
nombreuses modifications aussi bien sur le plan relationnel et social que sur
le plan technique.

C'est donc dans ce cadre de transformation que se déroule mon apprentissage, au
sein de l'équipe _Compute_. Équipe fondatrice de Scaleway, alors simple
expérience d'Online, Compute est en charge de la majorité des opérations et
développements : gestion des hyperviseurs et des APIs en relation. Les
_instances_\index{instance} représentent alors le seul produit disponible.  
Petit à petit, Compute voit son rôle diminuer, alors que des composants sont
migrés vers d'autres équipes. Les équipes _network_, _storage_ ou encore _CI-CD_\index{CI-CD}
prennent en effet à leur charge ces composants respectifs. Cette opération
permet à chaque équipe de se concentrer sur son cœur de métier, avec ses
propres spécificités, défis et besoins clients.

En tant qu'apprenti, il a été au premier abord compliqué d'appréhender la
technicité de l'infrastructure de Scaleway. Se positionnant sur le marché du
cloud comme marque innovante, il a donc fallu se former sur les technologies
les plus actuelles proposées sur le marché.  
Afin de fonctionner correctement, Scaleway met en place de nombreuses règles de
bonnes pratiques. Au départ hasardeuses avant la réorganisation, celles-ci sont
maintenant bien définies et permettent à chaque collaborateur de développer et
d'opérer dans les meilleures conditions possibles.  
Ces différentes pratiques incluant des _code review_\index{code review}, des tests ainsi que des
horaires précis de mise en production, permettent une cohésion d'ensemble
ainsi qu'un contrôle qualité.

_Compute_ a donc eu un besoin fort de DevOps\index{devops} dès sa réorganisation. En
intégrant cette équipe, de nombreux chantiers ont dû être engagés : ajouts
de fonctionnalités, stabilisation de l'infrastructure existante, déploiements
de nouveaux matériels, mise à jour et création d'images\index{image} serveurs
mais aussi maintien en condition opérationnelle de l'interne.

\vspace{15mm}

Les différentes tâches effectuées s'inscrivent dans le mouvement DevOps.
Contraction de _development_ et d'_operations_, un devops se voit ainsi
conférer des tâches très variées. Celles-ci peuvent en effet avoir trait au
développement ou à l'administration système seulement tout comme les deux
ensemble.  
Cette double compétence permet aux intéressés de comprendre toute la chaîne de
vie de leur projet mais aussi d'économiser en interactions coûteuses.

La majorité des projets auxquels j'ai pu prendre part suivent cependant
régulièrement la même trame :

* Analyse des besoins clients ou internes
* Analyse de l'existant
* Développement
* Contrôle qualité
* Reviews\index{code review}
* Mise en production

Malgré sa complexité, il faut parfois pouvoir reprendre le travail d'un autre.
Ces tâches doivent donc être bien définies et chaque partie documentée afin
d'en faciliter la compréhension par chaque membre de l'équipe.

Au sein de Scaleway, j'ai ainsi pu travailler sur de nombreux composants, dont
certains exposés directement au client. C'est le cas notamment de l'_api_compute_
\index{api-compute}, API exposant l'ensemble des projets _Compute_ au reste
de l'entreprise mais surtout des clients.  
C'est de ce point d'entrée que sont ensuite exécutées toutes les actions ayant
trait aux _instances_ : démarrage de serveur, attribution d'IP dynamique,
ajout de volume de stockage, modification des règles firewall, création de
snapshots\index{snapshot} et d'images\index{image}, etc.  
En travaillant sur ces composants extrêmement techniques, j'ai pu affûter mes
connaissances sur des technologies actuelles de pointe. Certaines
problématiques comme la haute disponibilité et le retour sur erreur imposent de
réfléchir à une architecture résiliente dès le début d'un développement.

C'est ainsi au sein d'équipes compétentes et très techniques qu'évoluent les
employés de Scaleway. Malgré ces aspects, la stack\index{stack technique} 
Scaleway reste compliquée à comprendre pour les nouveaux arrivants. De
nombreuses semaines de formation sont nécessaires pour en appréhender les
bases. Il n'est pas rare, au sein d'une même équipe, de ne pas comprendre 
certains aspects et de ne pas connaître toutes les ramifications d'un projet.


\newpage

# Introduction

## Rappel de la mission et des finalités de celle-ci

\index{docker}

Ma mission à Scaleway se classe dans l'écosystème des DevOps\index{devops}.
J'ai en effet pour responsabilité de nombreuses tâches ayant trait aussi bien
à l'administration système que le développement en passant par la résolution 
de bugs remontés par les clients et l'équipe de support

L'équipe _Compute_, dans laquelle j'opère a pour charge d'administrer et de 
développer le cœur historique de Scaleway : l'infrastructure technique cloud. 
Nous gérons en effet les différents composants permettant d'approvisionner les
hyperviseurs mais aussi les API permettant de lancer des actions sur ceux-ci.
Certains services annexes sur lesquelles je travaille sont également de notre 
ressort :

  * la marketplace\index{marketplace}, vitrine client pour le choix des images\index{image} (Ubuntu, Debian, 
    ...)
  * Build des images, permettre une création aisée desdites images et de 
  pouvoir les augmenter pour proposer des "instantapps" (Wordpress, Docker, 
  LEMP, Prestashop, ...)

Le développement ne constitue qu'une partie de notre travail au quotidien. 
Nous créons le plus souvent des scripts courts ayant pour finalité de régler
certains bugs, de remplir certains champs d'une base de données, de créer
des maintenances sur des hyperviseurs, etc.

Bien que certains codes développés soient courts, ceux-ci coûtent cher en 
temps. Les différents projets gérés sont en effet très techniques et y 
développer ne serait-ce qu'une fonctionnalité s'avère formateur et parfois 
long lorsque les connaissances ne sont pas encore acquises.

\newpage

## Présentation de l'entreprise

Scaleway, filiale du groupe Iliad (entre autres Free, Free Mobile), est un
acteur fort dans le domaine du cloud et du serveur dédié français. Crée en 1999
sous le nom de Online SAS, Scaleway est actuellement une entreprise comptant
plus de 200 employés, ayant effectué un pivot important au début de l'année
2018. C'est en effet plus de 150 recrutements qui ont été effectués cette
année, se répartissant sur les locaux de Paris, Lille, ainsi que nos
datacenters parisiens. L'entreprise cherche encore à acquérir de nouveaux
talents, avec environ 60 recrutements de prévus au cours de l'année 2019. Les
locaux ont eux aussi évolué, passant de 300 à 3 800m\textsuperscript{2}.

Scaleway possède actuellement 5 datacenters répartis dans la banlieue proche
parisienne. C'est ainsi qu'est formé un parc de plus de 150 000 serveurs dédiés
auxquels s'ajoutent les serveurs que nous possédons dans un datacenter à 
Amsterdam. Celui-ci permet de répondre aux besoins de connectivité importants
pour nos clients qui se répartissent dans plus de 150 pays.  
Le dernier datacenter en date, DC5, situé à Saint-Ouen-L’aumône, compte une 
superficie de 32 000m\textsuperscript{2} et une puissance électrique disponible
de 22MW. Ce datacenter novateur utilise un refroidissement d'air adiabatique, 
premier en Europe, le rendant attractif d'un point de vue RSE pour les grosses
entreprises. Cet investissement conséquent dans un nouveau datacenter permet 
donc une croissance dans la durée.  

D'autres datacenters sont prévus courant 2019, en Europe de l'Est mais aussi en
Asie afin de répondre aux besoins locaux, où peu d'autres cloud providers sont
actuellement présents.
Un de nos datacenters, DC4, situé dans un bunker anti-atomique, a, pour son cas
pour client final les entreprises désirant une sécurité accrue. Les projets
hautement critiques du gouvernement pourraient entre autres y être hébergés.


Scaleway se positionne dans le top 10 des clouds providers en Europe face à 
notamment 1&1, OVH ou encore DigitalOcean. Le marché français voit quant à lui
Scaleway se positionner dans le top 3. Son chiffre d'affaires était en 2017 de
70 millions d'euros dont 60 % à l'international, son placement y étant 48e.
Scaleway compte 400 000 clients réguliers, 60 000 actifs et 700 grands comptes.

Scaleway comporte de ce fait différentes marques :

* Scaleway, marque cloud proposant des services de catégorie Compute, Storage,
  Network et Artitifical Intelligence. On peut ainsi y dénombrer plusieurs 
  produits : Kubernetes, Instances\index{instance} (machines virtuelles), GPU Instances, 
  Baremetal (serveurs dédiés), Object et Block Storage, Cold Storage (C14),
  Load Balancers, Databases, Containers.
* Online by Scaleway, marque historique se positionnant dans le marché du 
  serveur dédié (Dedibox\index{dedibox}), du stockage et du Web Hosting tout en étant bureau
  d'enregistrement pour noms de domaines.
* Scaleway Datacenter, marque permettant la collocation ou encore le cloud 
  privé au sein de nos datacenters.

![Positionnement de Scaleway](./images/marketing_scw.png)

Le marketing B to B, Business to Business a pour clientèle les entreprises.
Celui-ci s'oppose au B to D (developers) ou B to C (consummers), qui vise quant
à lui les particuliers. 
Les clients type de nos infrastructures B to B sont ceux ayant un besoin de 
solutions sur mesure : les grandes entreprises, ETI, grosses PME et autres
hosters/software providers. Un SLA élevé, un support important sont ainsi 
assurés pour ces offres. Celles-ci sont gérées par une équipe dédiée de 
telesales. On peut retrouver parmi nos clients LeBonCoin, Vente-Privée, Le 
Monde ou encore Adobe.
Nos clients B to D en revanche sont plutôt les développeurs, TPE et revendeurs
locaux n'ayant pas besoin d'autant de services. La motivation desdites 
structures à commander chez Scaleway reste les prix bas.


Le pivot précédemment énoncé, ayant pour but de rendre Scaleway marque caution
sur laquelle la communication est faite, voit porter ses fruits lors de l'année
2018. Scaleway obtient en effet l'agrément Hébergeur de Données de Santé (HDS) 
lui permettant de proposer aux professionnels de santé mais aussi aux startups
des biotechnologies des hébergements sécurisés pour les données personnelles 
garantissant ainsi le respect de la vie privée et du secret médical. Scaleway
est également certifié ISO 27001 (sécurité) et 500001 (consommation
électrique).


\newpage

## Maturité de l’entreprise sur les thématiques de la mission

\index{bash}
\index{linux}
\index{python}
\index{sql}
\index{flask}
\index{celery}
\index{docker}

Scaleway a adopté les méthodologies agile pour gérer ses équipes. Celles-ci
sont orientées produit, on retrouve ainsi de nombreuses équipes en fonction de
l'évolution de la roadmap interne : AI, IoT, Compute, Storage, Database,
Network, User Accounts ou encore Billing.

Notre équipe, Compute, relève, par essence, d'une technicité élevée. Nous
sommes en effet amenés à gérer l'architecture, le développement et le maintien
en conditions opérationnelles de milliers de machines, de workers, d'images\index{image},
d'APIs et de bases de données.  
Les différentes connaissances requises sont donc très variées : Python, Celery, 
Linux, Flask, Docker, Bash et PostgreSQL, entre autres. Chaque élément possède
ses propres spécificités et de nombreux cas nous poussent à avoir recours à des
fonctionnalités très précises de chaque langage, bibliothèque ou logiciel.

Malgré une bonne connaissance des outils à notre disposition, il arrive tous
les jours d'être confronté à l'inconnu. Chacun, dans son équipe, devient alors
référent sur un sujet qu'il aura été seul, ou en binôme, à travailler et à
explorer en détail. Une personne minimum très souvent possède les 
connaissances requises à la tâche à effectuer. Si ce n'est pas le cas, 
plusieurs personnes y sont affectées.  

Scaleway compte à l'heure actuelle une part importante de développeurs Junior
comparée à celle des Seniors. Cette répartition permet une montée en
compétences rapide, même si parfois des décisions hâtives peuvent être prises
dues à ce manque d'expérience.

Mon premier maître d'apprentissage, Yann Léger, VP Compute, a, de par son 
poste, de nombreuses responsabilités. C'est en effet lui qui gère la
direction que dans laquelle les équipes doivent avancer. Que ce soit d'un
point de vue client, produit ou technique, nous nous coordonnons pour 
satisfaire aux besoins et exigences de l'entreprise. Faisant partie des
premiers employés de Scaleway, Yann possède une forte expertise aussi bien sur
l'infrastructure aussi, mais aussi dans le domaine du cloud et des aspects
techniques requis. Durant mon contrat, Yann nous a annoncé vouloir continuer à
développer son expertise au sein d'une autre entreprise ; cette décision a 
ainsi eue pour effet d'engendrer une autonomie plus forte qu'auparavant au sein
de notre équipe.
Notre équipe n'ayant plus de manager permanent officiel, c'est un collègue,
Thibault Billon qui a accepté de m'accompagner pour la fin de mon
apprentissage.

\newpage

## État des connaissances sur la mission chez l'apprenti

\index{bash}
\index{python}
\index{sql}
\index{docker}
\index{celery}
\index{linux}
\index{salt}
\index{awx}

Travaillant auparavant dans l'équipe Sécurité SI de la branche Service Courrier 
Colis de La Poste, j'ai par la suite rejoint Scaleway en janvier 2018 sur 
proposition d'un ami y travaillant. L'entreprise se restructurant, de nombreux
postes étaient à pourvoir et des apprentis pouvaient être embauchés.  
Ayant toujours été intéressé par les défis techniques et l'envie de progresser,
j'ai rejoint l'équipe Compute œuvrant au cœur de Scaleway.  
Mes connaissances relatives à la mission avant cette embauche me semblaient
satisfaisantes, j'avais en effet des connaissances correctes sur les systèmes
Linux, en Bash et Python.

J'ai par la suite, durant ma mission, appris que mes connaissances étaient loin
derrières celles de mes collègues, la plupart ayant des années d'ancienneté
aussi bien dans l'entreprise que dans celles du secteur.  
Le fait de travailler en équipe sur des sujets techniques se révèle très
formateur. Je n'avais jusqu'alors jamais travaillé à plusieurs sur des projets
en dehors du cadre universitaire.  
C'est ainsi que j'ai pu me former sur les différentes bonnes pratiques et 
coding styles, en vigueur dans l'entreprise mais aussi de manière plus globale
dans le monde. Scaleway essaye en effet de s'adapter aux bonnes pratiques des
différents projets open-source que l'on utilise.

Certaines connaissances ne peuvent être approfondies par soi-même, c'est
notamment le cas d'une infrastructure de plusieurs milliers d'hyperviseurs. Ce
scénario très précis m'a permis d'apprendre largement sur les systèmes Linux,
de la manière dont ils communiquent entre eux ainsi que la gestion des 
configurations. Cet apprentissage relève entre autres des langages utilisés 
(Python, Bash, Go) mais aussi des bibliothèques (Celery, Requests) et des 
langages de configuration spécifiques à chaque application (Salt, Docker, 
Ansible, Gitlab\index{gitlab}, Jenkins\index{jenkins}, etc.). 
De manière générale, la stack technique\index{stack technique} de
Scaleway étant très diversifiée et complexe, il est possible d'apprendre 
abondamment les spécificités de chaque élément et de l'écosystème dont il fait
partie.

Lors de l'élaboration de projets ambitieux tels que la synchronisation d'images
disque à travers différentes régions du monde, il fallut se pencher sur des
problèmes d'optimisation et de retour sur erreur. Cette nécessité, cette 
pression quant à avoir une stack résiliente m'a permis de développer mes
compétences d'architecture d'infrastructure.  
Ce même besoin de réactivité se fait ressentir pour le support client. L'équipe
en charge nous remonte en effet les problèmes qu'ils ne peuvent gérer par
eux-mêmes, ceux-ci sont ensuite traités dans les délais les plus brefs pour
minimiser l'impact client.


Les différents cours dispensés à EPITA\index{EPITA} sont eux aussi formateurs. Grâce à eux,
j'ai pu en effet comprendre plus aisément certains aspects de mon travail 
actuel.
Tous les cours n'ont en revanche pas une finalité technique mais plutôt de
méthodes d'apprentissage. Il m'est arrivé de devoir réapprendre des
connaissances transmises durant mes années de formation. Ces formations ayant
été efficaces, il m'a été bien plus rapide qu'à l'origine de retrouver les
éléments requis pour pouvoir avancer.
Il m'est également plus aisé aujourd'hui de comprendre les équipes non
techniques telles que les ressources humaines et le marketing. Les différents
cours d'EPITA\index{EPITA} couvrent en effet ces parties, permettant aux étudiants d'avoir
une vue d'ensemble de l'entreprise dans laquelle ils évoluent.

Mes motivations quant à ma mission sont diverses. J'ai en effet un besoin 
constant d'apprendre et de perfectionner mes connaissances techniques. Scaleway
est l'environnement idéal pour progresser vite et efficacement. J'ai par
ailleurs toujours voulu savoir comment marchaient les différents services
d'hosting et cloud en interne, ayant déjà été client au temps d'Online.


## Justification de l'intérêt et du positionnement de la mission pour l'entreprise

\index{python}
\index{c++}

Scaleway effectue un pivot fin 2017, créant ainsi de nombreux postes, aussi
bien à Paris que Lille. Cette création de postes rend nécessaire de recruter
des talents, tant juniors que seniors.  
Au moment de ma candidature, je ne savais pas quel poste je souhaitais 
réellement occuper. J'étais en effet attiré par le développement Python ou C++,
décrivant de fait de fait une bonne partie des postes proposés.

Scaleway, voyant ses employés se réorganiser au sein d'autres équipes,
nécessite une attention d'autant plus particulière qu'avant. Certains employés 
d'ancienneté plus grande et possédant de nombreuses connaissances, décident à 
ce moment de quitter l'entreprise afin de retrouver pour la majorité un cadre 
"startup" plus classique.  
C'est ainsi après mes entretiens que je me vois attribuer une mission au sein
de l'équipe Compute. Cette mission a pour objectif de consolider et développer
l'infrastructure au cœur de Scaleway.

Ne dérogeant pas à la règle, le travail de DevOps\index{devops} chez Scaleway est varié. Les
tâches sont régulièrement nouvelles, les compétences à y apporter également.
Travaillant sur des produits directement liés au client, il est aisé de voir
l'intérêt d'une telle mission : le développement de fonctionnalités et le fait
de traiter les bugs permet de consolider la base client en améliorant l'image
de marque. Suivant des plannings élaborés régulièrement, Scaleway s'assure de
toujours travailler sur des problématiques d'actualité. Les missions effectuées
se retrouvent ainsi en règle général pertinentes.

Dans le cas d'une non-satisfaction de la mission par l'employé, celui-ci peut
se réorienter. De nombreux collaborateurs ont ainsi évolué vers d'autres
équipes lorsque l'envie s'en faisait sentir. Outre ces changements, il est
possible d'avoir un rôle transverse, de nombreux projets s'interconnectant au
sein de l'entreprise. Une mission peut ainsi être reformulée par l'entreprise
au profit d'une autre, jugée plus appropriée pour son propre développement et
celui de ses employés.

\newpage

## Contexte précis de travail

### Lieu de travail

Scaleway opère dans un secteur en pleine croissance où les talents sont rares.
La demande étant faible largement inférieure à l'offre, il faut savoir mettre
en œuvre des moyens pour attirer ces personnes qualifiées.

Lors de leur arrivée dans l'entreprise, chaque collaborateur se voit remettre
différents _goodies_ : sweat-shirt, t-shirt, décapsuleur ou encore mug au nom
de Scaleway. Ces petites attentions, très ancrées dans la mode des startups 
récentes, permettent, dès l'arrivée, de détendre les employés en leur faisant
comprendre que le cadre de travail n'est pas celui des openspaces classiques.

Les locaux sont en effet très avantageux :

* décor exotique
* boissons gratuites : café, sodas, jus, bière et vin
* petit déjeuner et apéritif
* déjeuner peu cher et de qualité
* localisation centrale dans Paris
* liberté totale quant à l'emplacement de travail : flexoffice

![Possibilité de travailler où l'on souhaite](./images/scw_locaux.jpg)

Les différents points cités plus hauts sont à l'avantage du collaborateur, lui
permettant de s'épanouir pleinement lors de ses pauses ainsi qu'en travaillant.
Un environnement de travail agréable permet une meilleure productivité. Le fait
de ne pas avoir de bureau, et donc de pouvoir choisir avec quelle équipe se
placer, favorise le dialogue entre celles-ci. Chaque personne n'est pas
restreinte à son cercle classique de quelques collègues. Ainsi, il est possible
de travailler avec la personne la plus compétente requise à la tâche en cours.

Nos locaux sont ouverts 7 jours sur 7 et 24h sur 24. Cela permet pour les plus
matinaux de profiter du petit-déjeuner et aux autres d'arriver plus tard.
Cette flexibilité dans les horaires de travail permet d'organiser au mieux
sa vie professionnelle par rapport à sa vie personnelle. S'il est nécessaire
de partir plus tôt ou d'arriver plus tard que d'habitude, cela est en effet
possible sans contrainte particulière.  
Un apéro étant disponible tous les jours à partir de 18h, on peut retrouver
ses collègues autour d'un verre pour discuter ou non de travail. On peut ainsi
dans ces conditions apprendre beaucoup sur l'ambiance et l'atmosphère régnant
dans l'entreprise et peut-être même débloquer certains projets en envisageant
des directions nouvelles.

Les différentes équipes sont réparties sur Paris et Lille. Il devient donc
nécessaire de pouvoir communiquer efficacement malgré la distance. Les
allers-retours en TGV sont pris en charge par l'entreprise au besoin.

### Moyens matériels et informatiques

Chaque employé se voit remettre un ordinateur portable à son arrivée : un
MacBook Pro ou un Lenovo ThinkPad. Ces ordinateurs, puissants, permettent à
chacun de travailler dans les meilleures conditions. J'ai pour ma part opté au
départ pour un MacBook Pro puis changé pour un ThinkPad, ayant besoin d'une
réelle distribution Linux pour accomplir au mieux mes tâches.
Des écrans 27 pouces sont disposés dans les locaux pour permettre à ceux qui en
ont besoin d'utiliser deux écrans.

![Écrans disposés dans les locaux](./images/scw_screens.jpg)

\index{pull-request}

L'ensemble du code source de l'entreprise est disponible sur un Gitlab\index{gitlab} interne.
Celui-ci recense en effet le code de chaque équipe et de ses projets ou outils.
Hors projets contenant des identifiants de production, chaque employé peut
consulter le code du projet qui l'intéresse. Il est ainsi très aisé de
comprendre comment fonctionne la partie que l'on souhaite intégrer ou augmenter
si elle appartient à une autre équipe.  
Il est possible de contribuer à n'importe quel projet via des Pull Requests.
Celles-ci sont ensuite revues par les mainteneurs du projet qui décident ou
non s'il est pertinent de les intégrer. De nombreux cas peuvent motiver
ces-dites Pull Requests : bugfix, features, commentaires, release, etc.

\index{confluence}

Pour gérer nos projets et suivre nos bugs, nous utilisons Jira\index{Jira}. Jira permet de
créer des _tasks_ par projet et de les assigner aux personnes concernées avec
une certaine priorité. Chaque employé peut ainsi créer une tâche et l'assigner
à la personne qui lui semble convenir. Cet outil nous donne la possibilité de
suivre l'avancement de chaque sous-partie d'un projet et d'être efficace dans
la réalisation de celui-ci. L'historique de chaque tâche complétée y est
conservé, il peut en effet être utile parfois de revenir sur d'anciennes tâches
et ses commentaires pour comprendre l'état actuel du SI.  
À Jira est lié Confluence, un outil de documentation. Celui-ci permet de lier
de la documentation à des projets et tâches. On peut ainsi voir facilement 
quelle tâche a engendré quelle documentation et inversement.

\index{slack}

La communication interne se fait principalement via Slack, une plateforme de
communication collaborative. À l'instar du protocole IRC, Slack permet la
création de canaux de discussion aussi bien publics que privés. Chaque 
collaborateur peut donc rejoindre les canaux publics qui lui semblent
pertinents quant à sa mission. J'ai par exemple rejoint les canaux ayant trait
à mon équipe mais aussi ceux du marketing ou du site web pour me tenir au
courant des nouvelles sur l'entreprise en général.  
Slack contient également des plugins, permettant d'interagir avec d'autres 
outils que nous utilisons. Les plugins Jira et Jenkins sont par exemple très
utilisés pour être notifié en instantané de l'évolution des tâches et des
builds logiciels.

\index{vpn}

Le WiFi de nos locaux n'est en rien particulier, il ne permet qu'un accès à
internet. Pour pouvoir se connecter aux machines internes, y compris celles
hébergeant la documentation et le code source, il faut utiliser un VPN.
Celui-ci permet d'avoir accès au parc interne de Scaleway et ne nécessite donc
pas d'être sur place pour pouvoir travailler. Ainsi, la réponse sur incident
se retrouve facilitée tout comme le télétravail.

\newpage

# Aspects organisationnels

## Découpage de la mission

Le métier de DevOps\index{devops}, par définition, est d'être apte de gérer un
large panel de logiciels, langages et configurations. Ainsi, il n'y a pas de
mission particulière de définie mais plutôt un ensemble de plus ou moins
longues tâches.  
Cette section définit les différentes étapes de ces tâches, la finalité de 
celles-ci ainsi que les différentes actions y étant liées.

### Management

Les journées d'un DevOps chez Scaleway sont rythmées par les tâches Jira\index{Jira} 
classées dans des _sprints_ de la méthode Agile. Cette méthode de management
se focalise sur la réalisation d'action sur un temps donné.  
Chaque tâche possède son nombre de points, la difficulté de sa réalisation,
ainsi que les personnes y étant assignées.  
Chaque jour, notre équipe se réunit pour un _standup_ physique, réunion courte
lors de laquelle nous parlons de nos réalisations de la veille, des problèmes
rencontrés ou des améliorations possibles ainsi que du travail prévu de la 
journée. Ce format permet de rester informé de l'avancement de ses collègues
sur les différents projets en cours.  
Un sprint planning est organisé avant chaque début de sprint. Ici, chaque
tâche existante est revue, estimée et planifiée ou non en fonction des
personnes disponibles et compétentes. Une réunion de clôture de sprint
a également lieu à la fin de celui-ci, au bout de deux semaines.

\begin{ganttchart}[
  x unit=.75cm
]{1}{14}
  \gantttitle{Répartition d'un sprint sur 2 semaines}{14} \\

  \ganttbar[name=saturday1-top,bar/.style={fill=none, draw=none}]{}{6}{6}
  \ganttbar[name=sunday1-top,bar/.style={fill=none, draw=none}]{}{7}{7}
  \ganttbar[name=saturday2-top,bar/.style={fill=none, draw=none}]{}{13}{13}
  \ganttbar[name=sunday2-top,bar/.style={fill=none, draw=none}]{}{14}{14}
  \gantttitlelist{1,...,14}{1} \\

  \ganttbar{Standups}{1}{1}
  \ganttbar{}{2}{2}
  \ganttbar{}{3}{3}
  \ganttbar{}{4}{4}
  \ganttbar{}{5}{5}
  \ganttbar{}{8}{8}
  \ganttbar{}{9}{9}
  \ganttbar{}{10}{10}
  \ganttbar{}{11}{11}
  \ganttbar{}{12}{12} \\

  \ganttbar{Sprint Planning}{1}{1} \\
  \ganttbar{Sprint Closing}{12}{12}

  \ganttbar[name=saturday1-bottom,bar/.style={fill=none, draw=none}]{}{6}{6}
  \ganttbar[name=sunday1-bottom,bar/.style={fill=none, draw=none}]{}{7}{7}
  \ganttbar[name=saturday2-bottom,bar/.style={fill=none, draw=none}]{}{13}{13}
  \ganttbar[name=sunday2-bottom,bar/.style={fill=none, draw=none}]{}{14}{14}

  \begin{scope}
    \draw [opacity=0.2,line width=12] (saturday1-top) -- ($(saturday1-bottom)+(0,-11pt)$);
    \draw [opacity=0.2,line width=12] (saturday2-top) -- ($(saturday2-bottom)+(0,-11pt)$);
    \draw [opacity=0.2,line width=12] (sunday1-top) -- ($(sunday1-bottom)+(0,-11pt)$);
    \draw [opacity=0.2,line width=12] (sunday2-top) -- ($(sunday2-bottom)+(0,-11pt)$);
  \end{scope}
\end{ganttchart}

### Développements

Parmi les tâches qui m'ont été confiées, nombre d'entre elles ont trait au
développement. Pour la plupart, ce sont des modifications d'API, de workers ou
la création de scripts qui sont nécessaires.

\begin{ganttchart}[
  x unit=.8cm
]{1}{12}
  \gantttitle{Découpage type d'un développement}{12} \\
  \ganttbar{Tâches Jira}{1}{12} \\
  \ganttbar{Analyse des besoins}{1}{2} \\
  \ganttbar{Spécifications}{3}{6} \\
  \ganttbar{Review}{5}{6} \\
  \ganttbar{Code et tests}{7}{10} \\
  \ganttbar{Code review}{9}{10} \\
  \ganttbar{Mise en production}{11}{12}
\end{ganttchart}

\index{code review}
\index{confluence}
\index{slack}
\index{salt}
\index{awx}

L'analyse des besoins se fait soit sur Slack, soit en présentiel avec les
membres compétents concernés. Les spécifications sont ensuite rédigées et mises
en ligne sur nos outils interne (Gitlab\index{gitlab} et Confluence) avant d'être revues pour
modifications s'il y a lieu d'en apporter. Viennent ensuite le code et les
tests de celui-ci, ces derniers sont obligatoires. Un code ne peut en effet
être mergé sur la branche master s'il n'est pas accompagné de tests.
Une fois le code testé et validé, il peut être envoyé en production. Les tâches
Jira\index{Jira} liées sont mises à jour tout au long de ce processus.

\begin{ganttchart}[
  x unit=1.2cm
]{1}{7}
  \gantttitle{Mise en production d'un package python}{7} \\
  \ganttbar{Git master branch}{1}{1} \\
  \ganttbar{Builds \& tests finaux}{2}{4} \\
  \ganttbar{Build virtualenv}{5}{5} \\
  \ganttbar{Déploiement AWX}{6}{7}
\end{ganttchart}

Le code testé, validé et prêt à partir en production se trouve toujours sur la
branche git _master_ du projet concerné. Notre outil d'intégration continue,
Jenkins, se charge ensuite de build les packages et le virtual environment lié
lorsqu'il s'agit de Python. Les tests sont ici encore fois exécutés pour éviter
tout problème lié à une configuration locale des employés. Un déploiement peut
ensuite être effectué via AWX, version open-source d'Ansible Tower, permettant
d'exécuter des commandes sur un nombre prédéfini de machines. Différents
templates décrivant les différents workers, API ou autres packages sont
disponibles pour faire une release efficacement sur une ou plusieurs zones à la
fois.  
Notre gestionnaire de configuration, Salt, nous permet d'éditer de et déployer
les configurations de nos différents projets sur les machines souhaitées.
Il est parfois nécessaire de modifier celles-ci pour refléter un changement :
ajout d'endpoints appelés dans le code, changement de valeurs en dur, etc.  
Dans le cas d'ajout ou modification de champs dans une base de données, une
migration de celle-ci est nécessaire. Elle est alors lancée avant le
déploiement du projet concerné  

### Opérationnel

Certaines tâches en revanche, relèvent de l'opérationnel et non du
développement. Le processus n'est ici pas défini de manière générale car
différent pour chaque tâche. Il reste de rigueur que chaque modification doit
être testée avant d'être appliquée.  
De nombreux outils de monitoring tels qu'Icinga ou un dashboard interne nous
permettent de voir les problèmes en cours. Des scripts nous permettent ensuite
d'opérer sur le parc de Scaleway.

\begin{ganttchart}[
  x unit=1.0cm
]{1}{9}
  \gantttitle{Découpage de tâche opérationnelle}{9} \\
  \ganttbar{Tâches Jira}{1}{9} \\
  \ganttbar{Analyse des besoins}{1}{2} \\
  \ganttbar{Mise en œuvre locale}{3}{4} \\
  \ganttbar{Tests et reviews}{5}{7} \\
  \ganttbar{Application en production}{8}{9}
\end{ganttchart}

\index{code review}

\newpage

## Respect des délais et critique de ce découpage

Travaillant sur des projets aussi complexes que variés, il est parfois
compliqué d'estimer l'effort que peut prendre une tâche.  
Ainsi, malgré une attribution correcte des tâches en fonction des compétences
et des disponibilités, des délais supplémentaires peuvent être nécessaires.

Toute tâche, qu'elle ait trait au développement ou à de l'opérationnel, peut 
présenter son lot d'imprévu. Ne travaillant pas sur des projets longs, ces
imprévus constituent une part importante dans le temps déjà alloué à la tâche.
Les détails sont ainsi peu souvent respectés, l'estimation du temps nécessaire
étant extrêmement compliqué.

Avec l'expérience, il devient cependant plus aisé de faire cette estimation 
même si elle se révèle toujours approximative. La pression du management et le
besoin de la production nous force à faire des estimations basses, souvent peu
tenables.

De nombreux imprévus sur les outils internes pourraient cependant être évités
en se penchant sur leur résolution. Il arrive en effet de passer souvent du
temps sur le même problème sur l'environnement de développement ou de prod.
Un exemple équivoque est celui des workers de build Jenkins : ceux-ci sont
souvent surchargés et lents, rendant les tests et déploiements associés 
retardés.

## Nature et fréquence des points de contrôle en interne

Il existe différents points de contrôles pour les tâches attribuées à l'équipe.
Comme vu précédemment, des _standups_ sont organisés tous les jours. Lors de
ceux-ci, chacun peut prendre connaissance de l'état des tâches.  
Toutes les deux semaines, l'équipe se réunit pour faire un point final sur le
sprint et les tâches réalisés. Cela permet de réévaluer nos faiblesses et de
mieux organiser les nouveaux points ammenés à l'ordre du jour. Ces contrôles,
oraux, sont ainsi très importants pour la cohésion de l'équipe.

D'autres points de contrôle, dépendants des tâches, sont également imposés.
Notre équipe étant très autonome, je me suis principalement reposé sur celle-ci
pour faire contrôler mon travail à chacune de ces étapes. Les revues de code et
d'architecture sont en effet effectuées par son équipe ou celle possédant les
bonnes connaissances. Il peut ainsi y avoir plusieurs contrôles qualité par
jour comme aucun, dépendant des objectifs et tâches à réaliser.

Exceptionnellement, pour les longs projets, des démonstrations sont effectuées
devant l'équipe au complet. Une démonstration, par nature, n'est faisable
qu'une fois le projet déjà avancé. Ainsi, des revues préliminaires auront déjà
été faites avant toute présentation. L'intérêt est donc ici d'identifier les
faiblesses de l'architecture ou du code mis en œuvre avant d'envisager une mise
en production.


## Gestion des situations de crises, de problèmes techniques ou budgétaire ou politiques et relationnels

Comme toute entreprise dans le secteur de l'informatique, Scaleway est sujette
à des incidents affectant l'environnement de production. Deux grands cas de
figure sont à distinguer ici :

  * Les cas isolés, rencontrés par certains clients et gérés par le support
  * Les problèmes plus globaux, impactant l'infrastructure et ayant des
    retombées client

Au fil du temps, des incidents, et de l'expérience gagnée, une méthodologie
précise a été mise en place pour répondre aux problèmes impactant aussi bien
les clients que les services proposés par Scaleway.

### Le support chez Scaleway

\index{slack}

Scaleway fonctionne avec un système classique de tickets. Chaque client peut
ainsi envoyer sa requête, par mail ou téléphone, afin d'obtenir une assistance.
Dans le cas de problèmes simples ou souvent rencontrés, le support peut être
apte à répondre de lui-même. Pour les cas plus complexes, les équipes produit
sont sollicitées afin d'obtenir une réponse technique.  
L'équipe dans laquelle j'opère, _Compute_, met en place un système
d'astreinte : le _compute-doctor_. Chaque membre de l'équipe doit occuper ce
poste à son tour, c'est donc une dizaine d'employés qui se relaient chaque
semaine afin d'occuper le rôle de contact technique pour le support.  
Les demandes du support sont aussi bien effectuées par Slack que par Jira\index{Jira}. Les
moyens mis en œuvre pour résoudre les tickets sont obligatoirement notés pour
être mis en forme. Ces notes pourront ainsi servir lorsqu'une prochaine
remontée similaire se produira.


### Procédure de réponse sur incident

\index{slack}

La procédure de réponse sur incident prévoit premièrement d'en informer le
reste de l'entreprise via Slack, certaines équipes se reposant sur d'autres et
ayant besoin de connaître l'état du service. Une mise à jour sur l'incident et
sa résolution doit être fournie régulièrement, tout en modifiant les tickets
Jira associés en accordance. Une bonne communication permet une résolution plus
rapide, les personnes compétentes pouvant intervenir dans le cas idoine. La
résolution d'un incident ne peut être validée que par le CTO et les PMs
concernés, afin d'établir qu'aucun autre périmètre n'ait été impacté et que la
résolution appliquée est suffisante.  
Une salle dédiée, la _War Room_ est disponible dans nos locaux, dans le cas où
un incident nécessite de se concerter à plusieurs. Cette salle, équipée de
télévision connectée, permet de contacter les autres régions impactées pour
se coordonner.

Après résolution d'un incident, un _post_mortem_ doit être rédigé. Celui-ci a
pour vocation de garder une trace rédigée des causes de l'incident ainsi que
de la méthode de résolution. Toutes les opérations effectuées sont également
enregistrées, le but étant de ne plus subir d'interruption de nos services dans
le futur. 

\vspace{-10pt}

### Exemple d'incident

#### Oubli de renouvellement de certificat

Aussi bien en interne qu'externe, des certificats sont utilisés pour garantir
la sécurité des échanges. Ces certificats, ayant une date de validité, doivent
être périodiquement renouvelés Un oubli de renouvellement sur la gateway SMS
a eu pour cause de rendre inopérantes certaines requêtes.  
Les mesures correctives ont été de migrer ce projet vers un environnement de
production plus résilient, avec notamment un renouvellement automatique des
certificats.

\vspace{-10pt}

#### Coupure d'un lien fibre

\index{pelleteuse}

Début 2019, une coupure réseau affecte notre datacenter DC2. Cette coupure,
dont la cause reste non identifiée pendant plusieurs heures, trouve sa source
dans des travaux de la mairie à Ivry-sur-Seine. Travaillant sur un chantier du
tram, des employés ont en effet sectionné des fibres optiques avec leur
pelleteuse.  Cet incident, exceptionnel, mobilisa une grande partie de
l'entreprise. De nombreuses ressources internes étaient alors soit
inaccessibles, soit lentes. Cette fibre, reliant DC2 à DC3, n'a eu qu'un impact
client limité. Seules certaines lenteurs ont été notées.

![Incident dû à un coup de pelleteuse](./images/fibre.jpg)

Ce sont au final les équipes _network_ et _SRE_ qui réussissent à basculer les
liens réseau. Les jours suivants, de nouvelles fibres sont placées.

\newpage

# Aspects scientifiques, techniques et méthodologiques

Cette section décrit les aspects techniques, scientifiques et méthodologiques
mis en œuvre lors de la mission, ainsi que leur complexité. Les différents
outils utilisés ou développés pour accomplir les différentes tâches de la
mission sont détaillés et explicités ici.

\setcounter{tocdepth}{3}
\localtableofcontents

\newpage

## Outils utilisés

### Gitlab

\index{gitlab}

Gitlab est un outil phare dans le monde des DevOps\index{devops}. Celui-ci fournit en effet
de nombreuses fonctionnalités : suivi des bugs, wiki, intégration continue
mais surtout gestion de version.  
Nous l'utilisons chez Scaleway pour cette dernière fonctionnalité. Gitlab
permet, à la manière de GitHub ou Bitbucket, de visualiser les différents
_repositories_ git via une interface web et d'effectuer des actions sur
ceux-ci.

![Projet compute-workflows sur Gitlab](./images/compute-workflows-gitlab.png)

\index{pull-request}

Chaque utilisateur possède des droits sur les différents repositories. Ainsi,
il n'est pas possible pour un simple développeur de modifier le code de la
branche _master_ d'un projet. Pour ce faire, il faut passer par des
_pull-requests_ : une demande d'ajout des commits sur la branche visée, avec
explications des modifications. Une personne avec les accès adéquats peut
ensuite ajouter le code sur la branche et fermer cette pull-request.

La bonne pratique chez Scaleway est d'utiliser une branche de développement à
côté de la branche master. Chaque développeur, pour ajouter ses commits, doit
cependant toujours passer par des pull-requests pour ajouter son code sur la
branche _develop_. Des revues de code sont en effet obligatoirement effectuées
avant d'envoyer le code sur cette branche. Cette pratique permet de prendre
connaissance des dernières modifications mais aussi d'apporter son avis ou ses
recommandations sur le code.  
La branche master n'est utilisée que pour les différentes _releases_ d'un
projet. Les seuls commits additionnels par rapport à _develop_ sont ceux
décrivant cette release avec un journal des modifications (changelog).

\newpage
### Python

\index{python}

Python est le langage principal utilisé dans de nombreuses équipes chez
Scaleway. Dans l'équipe Compute, il représente plus de 470 000 lignes,
réparties sur les principaux workers et APIs.

De part son historique, Scaleway utilise toujours Python2.7 sur de nombreux
projets. Des efforts sont cependant en cours pour une migration vers Python3,
l'ancienne version arrivant en _end of life_ en 2020. Certaines migrations
n'ont pu être faites plus tôt du fait de l'incompatibilité des bibliothèques
utilisées avec Python3.

\vspace{-10pt}

#### Flask

\index{flask}

Flask est un framework de développement web open-source. Flask est populaire de
part sa simplicité et de ses capacités. Une application peut en effet être
développée et testée très rapidement via ses outils intégrés. Flask utilise le
moteur de template Jinja2, très répondu et également simple d'utilisation,
utilisant une syntaxe proche de celle de Python.  
Flask est cependant utilisé dans un contexte d'API REST chez Scaleway. De
nombreux plugins et supports existent ainsi pour cet usage : validateurs
(voluptuous), REST (flask-restful), base de données (sqlalchemy).

\index{sql}
\vspace{-10pt}

#### SQLAlchemy

\index{python}

SQLAlchemy est un ORM (object-relational mapping).Il permet ainsi de définir
des modèles de données en Python qui seront ensuite traduits en tables et
colonnes SQL. Une abstraction est ainsi faite du moteur de base de donnée,
seuls des objets Python sont gérés par l'utilisateur, il devient donc plus aisé
de manipuler les données.  
Une grande force de SQLAlchemy vient de son système de migrations. Il est
possible d'_upgrade_ et de _downgrade_ très facile sa base de données en ne
prenant en compte que les différences entre chaque version.

\vspace{-10pt}

#### Celery

\index{celery}
\index{flask}

Celery est un projet open-source de _task queue_ asynchrone. Il permet
d'exécuter des tâches en temps réel ou via un _scheduler_ pour des tâches
périodiques. Celery, couplé à RabbitMQ, permet de lancer toutes les tâches de
nos workers, par exemple le démarrage d'une machine virtuelle\index{instance}.  
Via un principe de _groups_ (exécution de tâches en parallèle), _graphs_
(dépendances entre tâches), il est possible d'exécuter de nombreuses actions en
parallèle tout en gardant une dépendance entre la complétion de chacunes
d'entre elles.

\vspace{-10pt}

#### Nose

Nose est une bibliothèque de tests. Celle-ci permet la capture des logs, des
sorties, l'introspection ainsi que la couverture du code.  
Pour chaque fonctionnalité développée, de nombreux tests doivent être écrits.
Nose permet en effet d'écrire très facilement des tests avec une approche objet
intuitive similaire aux builtins de Python (_assert_ entre autres).  
Pour ces tests, de nombreux services Scaleway sont utilisés en tant que
maquette, utilisant la plupart du temps des valeurs par défaut non pertinentes
pour la majorité des tests.

\newpage
### Gestion d'accès / LDAP

La gestion des utilisateurs peut s'avérer compliquée si elle n'est pas
réfléchie. L'utilisation de chaque application, site web ou service doit être 
régulée et restreinte pour éviter tout débordement.

LDAP, pour Lightweight Directory Access Protocol, est une ainsi norme pour les
systèmes d'annuaires. Un annuaire LDAP se représente sous forme d'arbre
stockant des clé-valeurs.  
Le but d'un LDAP est de simplifier la gestion des utilisateurs et l'accès aux
ressources. Ainsi, il est possible de créer des groupes d'utilisateurs, chacun
avec ses propres droits. Une majorité de logiciels s’interfaçant avec le
protocole LDAP, il devient possible d'utiliser d'un nombre restreint de mots
de passe au sein de l'entreprise.

Des clés SSH sont également stockés par notre LDAP. Celles-ci sont déployées
sur les machines d'infrastructure auxquelles l'utilisateur est censé avoir
accès.

Il arrive bien souvent de découvrir des services méconnus. Plutôt que de
demander des accès à l'équipe les maintenant, il suffit d'utiliser son compte
LDAP. Si les droits ne sont toutefois par existant, une requête doit être
faite à l'équipe _helpdesk_ qui jugera de l'utilité du service.

Toutefois, certains programmes ne s'interfacent pas avec notre LDAP. Chaque
collaborateur doit donc utiliser un gestionnaire de mots de passe personnel.
La gestion des utilisateurs sur ces programmes se fait donc manuellement et
peut engendrer des erreurs.  
Lors du départ d'un collaborateur, la suppression d'une entrée dans l'annuaire
suffit à lui faire perdre de nombreux privilèges. Il faut toutefois rester
vigilant dans les cas où l'annuaire n'est pas utilisé.

\newpage
### PostgreSQL

\index{sql}

PostgreSQL est un système de gestion de base de données open-source. PostgreSQL
est reconnu de par sa stabilité et ses performances.  
Chez Scaleway, PostgreSQL est aussi bien utilisé en interne que proposé aux
clients dans le produit _databases_.

Afin de répondre à ses besoins, Scaleway possède différentes bases de données,
réparties sur ses différentes régions. Il existe de ce fait majoritairement
trois ensemble de bases de données utilisées dans l'équipe Compute.

#### db-compute

Cette base, propre à chaque région, est utilisée pour les besoins des 
instances\index{instance}. On y retrouve entre autres les images\index{image}, snapshots\index{snapshot}, maintenances,
serveurs ou encore la description des disques au sein des services de stockage.


#### db-world

Unique pour toutes les zones, cette base sert de point central aux différents
services n'étant pas rattachés à une zone. On peut en effet y dénombrer des
services comme la _marketplace_\index{marketplace}, le _billing_ ou encore la gestion des
utilisateurs.

#### db-task

Cette table, relativement simple, n'est utilisée que pour stocker les
différentes tâches, Celery ou non, lancées par les APIs et workers. Cette base
de donnée permet ainsi de retrouver l'historique des tâches ainsi que leur
avancement. Cette base permet d'utiliser plus efficacement les tâches, étant
un mapping vers le _task backend_ de Celery.

\index{sql}

![Exemple de requête SQL sur les images marketplace](./images/postgresql.png)


\newpage 
### Jenkins

\index{jenkins}
\index{CI-CD}
\index{x86\_64}
\index{arm}
\index{arm64}

Jenkins est un outil open-source d'intégration continue possédant sa propre
interface web. L'intégration continue permet de s'assurer qu'un code développé
n'engendre aucune régression, ne casse aucun test.

![Build de l'API Compute sur Jenkins](./images/api-compute-jenkins.png)

Jenkins s'interface avec de nombreux logiciels de gestion de version, dans
notre cas : Git. Cette interface permet de lancer automatiquement des builds
à chaque modification du code source (à chaque commit) et donc de tester en
temps réel sur un environnement sain la conformité des programmes.  
L'avantage de cette intégration continue est donc d'obtenir en temps réel des
informations sur l'exécution réussie ou non des tests et de corriger au plus
vite les erreurs présentes. Les tests peuvent également être lancés de manière
périodique afin de vérifier qu'aucune dépendance au projet ne vient casser
le build.  
Ces différents builds et tests peuvent être exécutés sur différentes machines
(workers) afin de tester les spécificités de celles-ci. Chez Scaleway, nous
utilisons des machines aussi bien sur architecture x86_64, arm que arm64. Il
nous faut donc pouvoir tester ces trois architectures.

Ces builds sont exécutés sur des machines internes, mais il faut garder à
l'esprit que celles-ci ne sont pas des machines en production. Les seuls builds
effectués sur les machines de production sont les virtualenv des projets
Python. Ceux-ci doivent en effet être crées sur les machines qui vont ensuite
l'utiliser.  
Le processus habituel est donc de lancer builds et tests sur les branches de
développement et master, de build le virtualenv puis seulement de procéder au
déploiement. Les différents venv sont gardés et versionnés sur les machines, il
est donc possible de rollback la version en production très facilement, en cas
par exemple d'un bug de haute criticité.

\newpage
### AWX / Salt

\index{salt}
\index{awx}

AWX est la version opensource de Ansible Tower. Cet outil d'orchestration
permet d'exécuter une liste d'opérations sur un nombre donné de machines. Au
travers de _templates_, il est possible d'enregistrer les opérations
fréquemment lancées.

![Job template AWX](./images/awx.png)

Utilisés chez Scaleway pour les déploiements logiciels, ces templates listent
les différentes machines de l'infrastructure sur lesquelles tourne un
programme donné. Ainsi, chaque availability zone \index{availability zone}
apparaît lors d'un déploiement avec ses serveurs.

Cette granularité dans les déploiements permet de ne mettre à jour qu'une
partie du parc ou un seul composant à la fois. Outre le fait de pouvoir espacer
dans le temps une mise à jour, cela permet de déceler un potentiel problème au
plus vite, plutôt que celui-ci ne rende inaccessible toutes les zones.  
AWX garde un historique des différents jobs, il est donc possible de savoir
si un job échoue quelle configuration exacte a été utilisée.

\vspace{8mm}

Salt, programme opensource également, permet la gestion de configurations via
un modèle client-serveur. Les différentes configurations sont écrites en yaml
dans nos repositories Gitlab\index{gitlab} puis déployées via des commandes Salt inscrites
dans des templates AWX.  
Salt décrit un état de la machine, AWX permet quant à lui de lancer différentes
actions à l'aide de ses jobs. À eux deux, ces outils complémentaires permettent
des déploiements plus simple et automatisés pour notre équipe.

\newpage
### Docker

\index{docker}

Docker, logiciel libre, permet de lancer des _containers_. Ces containers
logiciels hébergent des applications qui sont ainsi non pas virtualisées, mais
conteneurisée. Cette approche, plutôt que de virtualiser un système complet,
se base sur des parties de la machine hôte existante, par exemple le kernel.  
Cette pratique permet un gain de performance tout en gardant la portabilité et
l'isolation des programmes.

Docker est utilisé dans de nombreuses parties de l'infrastructure de Scaleway.
Ainsi, comme vu précédemment, lors d'un build Jenkins, ceux-ci tournent sur
des machines internes. Celles-ci sont en réalité des containers Docker,
permettant d'émuler différents systèmes rapidement et facilement.

![Image officielle Gitlab présente sur le hub Docker](./images/gitlab.png)

La création d'images\index{image} serveur chez Scaleway se repose actuellement également sur
des containers. Des images cloud de distributions ou d'applications existent le
plus souvent déjà lorsque l'on souhaite en déployer de nouvelles. Il suffit
alors de récupérer ces images et d'ajouter les fonctionnalités propres à
Scaleway. Cela permet un gain de temps, de simplicité et de maintenabilité.

\vspace{-10pt}

#### Devcker

\index{docker}

Basé sur _Docker_, Devcker est un outil interne. Utilisé pour émuler toute 
notre stack\index{stack technique}, Devcker embarque La majorité des services
sur lesquels peuvent être amenés à travailler les développeurs : workers, APIs,
console web et bibliothèques.  
Cet outil de développement permet de travailler en local avec un code le plus
à jour possible. Les modifications apportées sur souvent plusieurs projets à la
fois sont ainsi rapidement exécutées.  
Cet outil nous permet donc de constituer des tests et prototypes avant de les
tester sur un environnement de pré-production.

\newpage
### RabbitMQ

RabbitMQ est un _message broker_ open-source. Les message brokers sont des
programmes intermédiaires permettant d'échanger des messages entre deux
applications de manière asynchrone.  
RabbitMQ fonctionne sur le principe de _publish-subscribe_. Des programmes
(appelés clients) se connectent et créent une _queue_, une file de messages. Il
devient alors possible d'envoyer des messages sur cette queue, qui seront
ensuite consommés par les différents clients enregistrés. Ce système permet
ainsi de répartir la charge, d'avoir un retour sur erreur et de router un
message vers différents clients. Le traitement des messages étant asynchrone,
il n'y a pas de point de blocage lié à l'attente de fin d'une tâche.

![Queues RabbitMQ](./images/rabbitmq.png)

\index{celery}

RabbitMQ peut être utilisé avec de nombreux clients. Chez Scaleway, nous
utilisons Celery pour consommer et publier des tâches.  Ces tâches, peuvent
être créées aussi bien par les différentes API que par les workers, parfois à
la suite.  
Par exemple, un démarrage d'un serveur s'opère via une requête _POST_ sur
l'api-compute\index{api-compute}, qui va ensuite lancer une tâche pour démarrer le worker
correspondant à l'architecture demandée. Ce worker, lancera à son tour
différentes tâches pour gérer entre autres le stockage.

\newpage
### Jira

Jira\index{Jira} est un programme de suivi de bugs, d'incidents mais aussi de gestion de
projet Jira permet en effet dans un premier temps de créer des _tickets_
décrivant des tâches, actions à effectuer. Ceux-ci appartiennent à un projet
et sont associés à une personne avec une criticité.

Il est ensuite possible d'affecter cette tâche à un _sprint_ qui sera démarré
par le manager de l'équipe concernée. Jira permet ici de suivre l'avancement
d'un projet sur une période donnée. Chaque tâche peut également se voir définit
un workflow sur mesure, adapté aux actions à réaliser, par exemple : 

* Open, la tâche vient d'être créée
* In Progress, une personne vient de commencer sa résolution
* Needs Review, la tâche est terminée et requiert une vérification
* Needs Qualification, le livrable est prêt à être déployé
* Done, la tâche est complètement terminée

![Description d'une tâche Jira](./images/jira.png)

Afin d'être le plus efficace possible, chaque action à réaliser passe par la
création d'un ticket Jira si celui-ci n'existe pas. Cela permet de suivre les
différents incidents et actions à opérer dans l'entreprise.  
Les tickets étant publics en interne, chacun peut y apporter une information
et son aide. Il est en effet fréquent de commenter sur une remontée de bug du
support ou sur le mode opératoire d'un développement à adopter.

\newpage
### Confluence

\index{confluence}

Confluence est un logiciel de wiki ayant la particularité d'être axé sur la
collaboration. Confluence étant édité par Atlassian, les créateurs de Jira,
ceux-ci s'interfacent naturellement entre eux.  
Comme la majorité des logiciels de wiki, Confluence permet la gestion
d'historique des modifications, l'intégration à un annuaire LDAP,
l'installation de nombreux plugins (votes, formulaires, graphiques, etc.).

![Documentation sur Confluence](./images/confluence.png)

La majorité de la documentation Scaleway réside ainsi dans Confluence. Ce
système permet un accès rapide à celle-ci via les recherches mais aussi une
interface efficace avec Jira\index{Jira}.  
Une procédure peut en effet être décrite sur Confluence et être ensuite
mentionnée dans Jira. Les différents tickets ayant trait à un processus
particulier peuvent ensuite être regroupés sur une page pour obtenir les
différents status.

![Intégration de Confluence à Jira](./images/jira_confluence.png)

À la manière de Jira, Confluence propose le téléversement de fichiers ainsi
qu'une version mobile, renforçant son accessibilité.

\newpage
### Sentry

Sentry est un programme open-source de suivi d'erreurs en temps réel. Sentry,
déployé en production, permet aux développeurs d'être avertis des erreurs
occurrant sur le parc de Scaleway. Ces erreurs sont envoyées par mail aux
personnes concernées dès leur apparition.
Les erreurs des différents services peuvent en effet être remontées : les
workers mais aussi les APIs. Toute erreur est accompagnée des informations
liées à son écosystème. Ainsi, on peut observer la version du service analysé,
les fichiers ainsi que la ligne incriminée accompagnés de la requête fautive :
l'url, l'endpoint visé, l'IP source et de destination.

![Dashboard Sentry](./images/sentry.png)

L'intérêt de Sentry est donc complémentaire à celui des tests unitaires.
Ceux-ci ne peuvent en effet être totalement exhaustifs, une erreur sera 
fatalement présente et un système permettant de les détecter est obligatoire.

Différents niveaux d'enregistrement peuvent être mis en place. Sentry peut en
effet remonter tout type de logs, que ce soit de l'information, du debug, des
warnings ou encore des erreurs.

Certaines de ces erreurs ne peuvent également pas être testées, ayant trait
à des services externes tels que la base de données ou d'autres APIs.  
Sentry nous permet donc dans ce cas de détecter ces problèmes, remontés par
d'autres services, comme des problèmes de connexion.

\newpage
## Outils internes utilisés

### Console d'administration

La Console d'administration Scaleway est un des outils les plus importants pour
gérer le parc de machines virtuelles\index{instance}. Cette console permet en effet
d'administrer et d'obtenir de nombreuses informations sur les hyperviseurs et
machines clients.

![Administration d'un hyperviseur et de ses Machines virtuelles](./images/console-adm.png)

Parmi les fonctionnalités possibles, on retrouve toutes celles opérables par un
client :

  * Création d'une machine 
  * Arrêt, redémarrage et démarrage d'une machine
  * Changement de l'IP publique
  * Ajout ou suppression de volumes de stockage
  * Création de snapshots\index{snapshot} ou d'images\index{image} disque
  * Création de Security Groups (règles de pare-feu)

La version d'administration intègre ainsi d'autres possibilités : 

  * Choix de l'hyperviseur pour une machine virtuelle
  * Mise en maintenance des hyperviseurs ou nodes
  * Inventaire des disques physiques (SANs)
  * Modification des bootscripts (kernel et initrd en réseau)
  * Modification des utilisateurs avec leurs quotas associés
  * Vue sur les factures client

Cette console est un outil rapide et efficace pour gérer les clients et les
services directement associés. Pour certaines tâches, il convient en revanche
d'utiliser d'autres outils ou de se connecter directement sur les machines de
production, dont l'adresse peut être trouvée sur la console.

\newpage
## Tâches effectuées

### Protection de reboot par inadvertance

L'ensemble des services de Scaleway peuvent être administrés via API. La
console en ligne n'effectue en effet que des appels vers ces APIs pour obtenir
des informations et effectuer des actions. Il est ainsi possible d'arrêter,
d'archiver ou de redémarrer un serveur via une requête _POST_.  
L'intérêt est ici de pouvoir automatiser la gestion d'instances\index{instance} via des
scripts. Il se peut cependant qu'une demande de reboot soit faite par
inadvertance, soit par manque de tests et bug quelconque, soit via lancement
d'un mauvais script.

Le but de cette fonctionnalité est d'ajouter un _flag_ sur une instance pour la
marquer comme protégée. Une fois ce flag activé, l'instance ne peut plus être
arrêtée ou redémarrée par l'utilisateur. Celui-ci doit être désactivé pour
redonner un comportement standard aux actions demandées.

![Interface de gestion d'un serveur](./images/protected_server.png)

Différentes modifications ont donc dû être apportées à l'api de gestion des
serveurs, l'api-compute\index{api-compute} :

  * Autoriser l'ajout du flag _protected_ lors d'un _POST_, _PUT_ ou _PATCH_ 
    sur l'endpoint serveur
  * Empêcher le reboot si le flag est présent lors d'un _POST_ sur l'endpoint
    d'action serveur
  * Modifier les actions possibles sur un serveur en fonction du flag
  * Ajouter le statut du flag lors d'un _GET_ sur l'endpoint serveur

Un serveur peut être crée via les méthodes _POST_ ou _PUT_, permettant
respectivement la création d'un objet soit via un nombre minimum de paramètre,
soit avec l'ensemble de ceux-ci. Un seul endpoint est disponible pour la 
création de serveurs : _/servers/_. Il faut ici donc ajouter
le flag _protected_, de type booléen au validateur. Celui-ci vérifie les
paramètres envoyés par l'utilisateur d'un point de vue typage, taille ou nom.  
Le paramètre _protected_ est optionnel, afin de ne pas engendrer de
_breaking change_ sur l'API. S'il n'est pas fourni, il est mis à _false_ par
défaut.  
L'intérêt de la méthode _PATCH_ est quant à elle de permettre la modification
d'un champ spécifique d'un objet sans devoir renvoyer l'intégralité des champs, 
comme la méthode _PUT_. Ici, tous les champs sont donc marqués optionnels.

Il est possible d'effectuer des actions sur un serveur via l'endpoint
_/servers/{server_id}/actions/_. Cette modification est
simple : lors d'une action _reboot_, _terminate_ ou _poweroff_, il faut vérifier
l'état du flag. S'il est actif, l'API doit renvoyer une erreur 400 : BadRequest,
l'opération n'étant pas permise. Si le flag n'est pas actif, l'opération est
exécutée normalement.  
Il y a toutefois une vérification des permissions de l'utilisateur qui doit
être effectuée. Un admin doit en effet être capable d'agir sur un serveur dans
le cadre du support par exemple. Ainsi, si l'utilisateur a le droit d'effectuer
des modifications sur tous les serveurs Scaleway, il est ici considéré comme
admin.

Il est possible d'obtenir une liste des actions disponibles sur un serveur lors
d'un _GET_ sur _/servers/{server_id}_, retournée dans un champ _allowedActions_.
Cette liste ne doit donc pas renvoyer les actions _reboot_, _terminate_ et
_poweroff_ si le flag est présent.  
Le statut du flag _protected_ est lui également renvoyé afin de pouvoir le
prendre en compte dans des scripts ou bien la console client.

La console d'administration n'étant peu ou plus gérée par l'équipe front de par
leur implication dans la nouvelle console client, il a été nécessaire d'ajouter
par nous-même cette nouvelle fonctionnalité.  
N'ayant aucune connaissance en Coffee ou Jade, il m'a été compliqué au départ
de procéder à ces changements. Il m'a en effet fallu lire beaucoup de code au
sein du projet ainsi que de la documentation avant de pouvoir développer ne 
serait-ce que quelques lignes.  
Ainsi, un bouton permet dorénavant d'activer ou désactiver la protection d'un
serveur dans la vue de celui-ci ; cette information se retrouve également dans
la vue complète des serveurs sous forme d'un petit cadenas.

Cette fonctionnalité a suscité quelques questions de la part des clients à sa
sortie. Un manque de documentation les a amenés à se demander quel était le
vrai intérêt de cette modification. Le terme "protection de serveur" peut
prêter à confusion, il n'était ici en effet pas question de protection
anti-virus automatique, firewall ou autre.  
Ce problème a pu être réglé en ajoutant des informations dans la documentation
mais aussi dans la description du bouton sur l'interface client.

\newpage
### Reboot automatique d'hyperviseur

\index{x86\_64}
\index{arm}
\index{arm64}

Scaleway possède des hyperviseurs déployés sur plusieurs architectures : x86_64
et ARM64. Il est ainsi possible de créer des machines virtuelles\index{instance} sur ces deux
architectures en plus des serveurs dédiés ARM.

Afin de pouvoir gérer le parc de machines fonctionnelles par rapport à celles
présentant des erreurs, il existe des maintenances. Ces maintenances permettent
d'indiquer sur une plage de temps une erreur et d'empêcher les clients de
démarrer dessus via une vérification de l'allocateur.  
Cette fonctionnalité permet aussi bien de repérer les hyperviseurs ou matériels
à problèmes mais aussi de réserver des machines pour des tests.

Les hyperviseurs ARM64, utilisant Ubuntu, sont en général très stables. Il
arrive cependant que ceux-ci plantent. D'expérience, un redémarrage de la
machine et des machines virtuelles liées est en général suffisant. Il a donc
été décidé d'automatiser la gestion des maintenances sur ces hyperviseurs.

![Maintenances sur un hyperviseur](./images/maintenances.png)

Il existe déjà un composant effectuant des migrations sur les hyperviseurs en
maintenance : _compute-workflows_. Ce _worker_ repère certaines maintenances
sur des hyperviseurs non fonctionnels et migre les clients vers d'autres
machines tout en les notifiant.  
Il a donc fallu ici ajouter le support d'un autre type de maintenance,
simplement nommé _stuck_hv_.

Différents éléments sont ici impliqués : les outils du monitoring avec
Prometheus, la base de données _compute_, l'api d'administration _compute_,
l'hyperviseur concerné et le worker _compute-workflows_. Ce worker s'exécute
périodiquement à l'aide du planificateur de tâches _cron_.

![Automatisation du reboot d'hyperviseurs](./images/workflows.png)

Redémarrer un hyperviseur est rapide : une simple requête _POST_ sur
l'api-compute\index{api-compute}-admin. Être certain de n'avoir aucun problème sur toute la chaîne
d'opérations est cependant plus compliqué.  
Des complications peuvent apparaître dès le début : le monitoring doit créer
une maintenance sur nos hyperviseurs. Un flap réseau par exemple ne doit pas
déclencher d'alerte, un seuil a dû être fixé. Ce seuil est ensuite re-vérifié
sur le worker _compute-workflows_, si celui-ci voit plus de N maintenances sur
des hyperviseurs ARM64, celles-ci ne sont pas traitées.  
À chaque opération, la maintenance est mise à jour avec de nouvelles
informations : date et nombre de redémarrages. Ainsi, le worker quitte si la
maintenance existe mais que l'hyperviseur a été redémarré il y a moins de
X minutes. Cette vérification n'était pas originellement prévue, ce n'est que
lors d'une présentation du projet à l'équipe que ce cas problématique a été mis
en évidence.

Dans le cas d'un nombre trop important de redémarrages, le worker assume que
l'hyperviseur a une défaillance plus importante que prévue. La maintenance
n'est donc plus traitée, une intervention manuelle est requise.  
Dans le cas où le monitoring indique un hyperviseur démarré, il suffit de
fermer la maintenance. L'hyperviseur pourra ensuite être utilisé par les
clients sans besoin d'intervention humaine.

Après avoir testé le code, il a fallu mettre à jour la configuration du worker
pour prendre en compte les appels à l'api d'administration. Un token a donc dû
être ajouté et l'endpoint renseigné dans la configuration.

\newpage
### Ajout d'un filtre sur le type d'un serveur

La console d'administration permet entre autres l'affichage de l'ensemble
des instances\index{instance} Scaleway réparties sur les différentes zones. Que ces instances
soient démarrées ou non, cette vue permet de repérer des erreurs sur leur
statut, volumes ou encore hyperviseur.

Dans le cadre de la lutte contre la fraude, l'équipe _Trust and Safety_
requiert de pouvoir filtrer les instances par type. L'api-compute\index{api-compute} ne propose en
effet pas cette fonctionnalité, ne permettant actuellement que le filtrage
sur le statut ou l'utilisateur.  
L'intérêt est ici de repérer les consommateurs de grosses ressources.

La solution la plus simple à mettre en place est d'ajouter une querystring
sur l'endpoint _/servers/_ de l'API.  
Cet endpoint effectue une validation du nom du serveur par rapport au catalogue
produit. Une fois la requête validée, les serveurs sont listés puis filtrés
en SQL en accord

\index{sql}

![Listing des serveurs d'un utilisateur](./images/servers_filter.png)

Une fois la partie API codée et déployée, il faut mettre à jour la console
d'administration. L'équipe _front_ n'étant pas assez nombreuse pour gérer le
projet, nous avons dû nous en charger.  
N'ayant aucune connaissance en Coffee et Jade, il a été fastidieux d'intégrer
ces changements.  
Peu après la mise en production de ces changements, il a été remarqué que la
pagination ne fonctionnait pas sur l'interface. Ce problème est dû à la
mauvaise compréhension du fonctionnement de la console. Une solution de 
contournement proposée a été d'intégrer les types de serveurs en dur, et non
plus dynamiquement.  
Cette console ayant vocation à être remplacée plus tard, il a été accepté de
garder cette solution, en fin de compte temporaire.

\newpage
### Ajout d'une liste de serveurs supportés sur la marketplace

\index{x86\_64}
\index{arm}
\index{arm64}

La marketplace\index{marketplace} Scaleway est la vitrine d'images\index{image} disponibles pour démarrer un
serveur. Elle expose les _ids_ des différentes images compute selon leur zone
(Amsterdam, Paris, ...) ainsi que leur architecture. Les serveurs proposés
étant différent, il est nécessaire d'avoir à disposition leurs spécificités
pour connaître les images compatibles. À l'origine, seule l'architecture était
importante, Scaleway proposant aussi bien de l'ARM, ARM64 que du x86_64.

Les volumes les plus petits proposés étant de 50Go, les images avaient cette 
taille également. Lors d'un démarrage, les images n'étaient pas redimensionnées
à la taille du volume, il fallait donc créer des images de cette taille.

À l'arrivée de nouvelles offres avec des volumes de 25Go, il a fallu cependant
créer des images plus petites. Le raccourci qui a été pris a donc été de créer
des images de 25Go avec un nom spécifique (par exemple "Ubuntu Xenial 25G". La
console client a donc dû adapter en dur dans son code la correspondance des 
images 25Go avec les serveurs 25Go.  
Avec le panel d'offres grandissant, il est devenu nécessaire d'avoir une solution
à long terme. Cette fonctionnalité permet donc de discriminer les images
compatibles par rapport aux serveurs.

Une idée a donc été étudiée : ajouter une liste de serveurs compatibles pour
chaque image dans la marketplace\index{marketplace}. Cette fonctionnalité
permet à la console et aux clients d'avoir une claire indication du serveur sur
lequel peut tourner une image.  
Une alternative aurait été d'ajouter ces possibilités dans les images du côté
compute, le calcul aurait cependant été plus difficile, une requête
supplémentaire devant être faite pour chaque image, de l'ordre de 150 requêtes
en tout par client.  
Deux modifications ont donc dû être apportées : à l'api-marketplace ainsi qu'à
la base de donnée qui lui est liée.

#### api-marketplace

Lors d'un _POST_ ou _PATCH_ sur l'API, un check d'architecture est effectué
pour vérifier rudimentairement les données envoyées. Les serveurs, sous forme
de string, sont ensuite ajoutés dans la liste _compatible_commercial_types_ qui
sera exposée aux clients sur un _GET_.

Il a été défini pour les données de cette liste que :

```
  liste vide -> aucun serveur compatible
  null -> aucune restriction de compatibilité
  [serv1, serv2] -> compatible avec serv1 et serv2 uniquement
```

Le paramètre _compatible_commercial_types_ étant obligatoire, cela engendre un
changement majeur de version sur l'api. Tous les tests et scripts utilisant
cette API pour créer des images ont ainsi dû être modifiés. Ceux-ci étant peu
nombreux, il est acceptable d'avoir procédé de la sorte, rendant l'utilisation
de cette fonctionnalité obligatoire et permettant ainsi de pérenniser une bonne
utilisation de la marketplace\index{marketplace}.

Les différents tests écrits prennent en compte : une mauvaise architecture pour
un serveur donné, des serveurs inexistants ainsi que les cas corrects
d'utilisation définis plus haut.

#### db-world

La base de donnée a elle aussi dû être modifiée en conséquence. La modification
a ici été simple : ajouter un tableau de string dans le modèle d'une image :

```python
  compatible_commercial_types = Column(Array(String(512)), nullable=True)
```

Scaleway utilise un principe de Factory : des modèles pré-définis avec
certaines valeurs pour pouvoir être utilisés facilement dans les tests. Les
serveurs compatibles y sont donc ajoutés à la création d'une image\index{image} dans les
tests.

#### Remplissage des valeurs

Après avoir ajouté la possibilité de filtrer les serveurs, il faut y ajouter
les serveurs compatibles en eux-mêmes. Un script a donc été écrit, ayant pour
finalité d'être exécuté après chaque ajout d'offre commerciale, changement de
nom ou ajout d'image\index{image}.  
Ce script vérifie comme auparavant l'architecture, mais aussi la taille de
l'image, du volume principal du serveur et des volumes au total. Il existe deux
contraintes : celle de taille par volume et celle totale.

Ainsi, une image de 10Go peut être démarrée sur un serveur de 25Go, mais non
l'inverse. Une image de 50Go peut être démarrée sur un serveur dont la capacité
est de 50Go, mais pas si celui-ci a une contrainte de 30Go par volume par
exemple.

\index{python}

Scaleway, pour ses différents projets, utilise un catalogue de produits : une
bibliothèque python. Ce catalogue permet d'obtenir les différents serveurs
ainsi que leurs caractéristiques selon les zones disponibles. Il ne reste plus
qu'à faire la liste selon les contraintes voulues.

Les différentes évolutions possibles sont d'automatiser ce script soit dans
l'API, une crontab ou autre. Certaines spécificités pourraient également être
ajoutées sur le modèle des serveurs et des images. Ceux-ci pourraient en effet
donner plus d'informations sur le type d'image qui est attendu : par exemple
boot EFI seulement ou encore SELINUX seulement autorisé.

\newpage
### Ajout d'un rang sur les images de la marketplace

Comme toute API REST, la marketplace\index{marketplace} ne renvoie pas ses objets dans un ordre
pré-défini. Ce comportement engendre un problème du côté console client : il
est impossible de savoir quel ordre utiliser pour mettre en avant les images\index{image}.
Pour des raisons marketing, il convient en effet de montrer aux utilisateurs
les images les plus populaires en premier, plutôt qu'à la fin.  
Sans service externe, qui serait lourd à déployer, la seule solution est donc
d'intégrer une liste d'images dans la console client. Cette liste, en dur dans
le code, n'est donc pas évolutive. Si, pour des raisons marketing ou
techniques, il convient de changer cet ordre, la console doit être redéployée.

![Images triées sur la marketplace](./images/images_mkt.png)

La solution à implémenter a donc été de trier le retour de l'api marketplace.
Différentes propositions ont été étudiées : 

* Ajouter un champ donnant le rang d'une image sous forme d'entier
* Ajouter un champ de popularité calculé dynamiquement

La première solution, bien que simple, ne permet pas de modification rapide du
rang en cas d'ajout ou suppression d'image, lors d'une mise à jour par exemple.  
Il a donc été choisi d'implémenter la seconde solution, le fait de mettre des
valeurs en dur étant peu apprécié par les développeurs.

Un camp _raw_popularity_ a été ajouté dans la table _MarketplaceImage_, celui-ci
donne une popularité de base à une image. Ainsi, une image Ubuntu aura une
priorité plus élevée qu'une Archlinux, basée sur nos statistiques 
d'utilisation.  
Une propriété hybride, propre à SQLAlchemy, permet d'avoir un champ
_popularity_ calculé dynamiquement, non stocké en base et utilisable en python.
Ainsi, il est possible de trier les images en SQL avant d'envoyer la réponse
de la requête.

Il avait été initialement proposé d'avoir une méthode _POST_ pour changer
cette popularité brute. Il a toutefois été décidé de ne pas exposer ce
paramètre aux clients, aussi bien sur un _GET_ que _POST_, car ce paramètre n'a
pas vocation d'être modifié. Cette valeur doit donc être modifiée via la base
de donne à l'aide d'un script ou de requêtes SQL.

\index{sql}

```SQL
UPDATE marketplace_image 
  SET raw_popularity = 1400 
  WHERE key = 'c0649a2a-e6bf-4712-9303-8d967153209c'
;
```

Chaque image est séparée d'un intervalle de 100 de la suivante. Cela laisse la
possibilité d'ajouter d'autres images sans déranger l'ordre établi.

La propriété hybride _popularity_ ne renvoie pour l'instant que la valeur de 
_raw_popularity_. Il convient dans le futur de créer un algorithme permettant
de pondérer ces rangs en fonction de différents paramètres :

* date de mise à jour
* usage en temps réel sur nos instances\index{instance}
* campagne marketing

Cette fonctionnalité permet donc de régler le problème rencontré par l'équipe
front des valeurs en dur. L'ordre des images est ainsi maintenant géré par la
marketplace\index{marketplace} ; il devient plus aisé de maintenir cet ordre, ne necessitant pas
de déploiement.

\newpage
### Build d'images

\index{jenkins}

Une instance Scaleway a besoin d'une image pour démarrer. Une image est
simplement un fichier _qcow2_\index{qcow}, associé à QEMU\index{QEMU}, couplé avec des metadatas. Une
image peut donc être démarrée sur une certaine machine, des volumes de certaine
taille, une certaine architecture. Lors de la création de ces images, il faut
donc vérifier certaines contraintes.

Pour créer les images Scaleway, nous utilisons encore une fois Jenkins. Les
images étant publiques, nous utilisons GitHub pour accueillir le code des
images, mais aussi des différents scripts utilisés. Un utilisateur peut donc,
lui aussi créer des images de la même manière qu'en interne. Le système actuel
de création d'images repose sur docker.

Garder des images à jour s'avère difficile : chaque mise à jour d'une
distribution ou d'une application nécessite des modifications, parfois
compliquées. Il faut parfois revoir l'architecture complète de l'image afin de
prendre en compte les mises à jour. Une image peut ainsi être mise à jour en 
moins de dix minutes, comme prendre plusieurs jours.

Notre système de tests sur Jenkins nous permet de valider des généralités sur
les images :

* l'image démarre-t-elle ?
* est-elle accessible en ssh ?
* le serveur web répond-il ?
* un package est-il bien installé ?

![Étapes de build d'image sur Jenkins](./images/nextcloud.png)

Chaque image\index{image} étant cependant si spécifique, il devient compliqué
d'automatiser davantage de tests. Les applications n'étant pas mises à jour si
régulièrement, il n'est pas nécessaire de s'encombrer avec un système complexe
et difficile à maintenir. Les mises à jour et vérifications se font donc
manuellement, souvent plus efficacement qu'avec des tests.

\newpage

#### Exemple de NextCloud

\index{jenkins}
\index{image}

L'ensemble des images proposées par Scaleway étant disponibles sur GitHub,
NextCloud y a ainsi été créée via le namespace _scaleway-community_.

Pour créer une image de ce type il a fallu réunir différents éléments :

* les dépendances de NextCloud
* où récupérer les sources de NextCloud
* quels services configurer

Lire extensivement la documentation de NextCloud s'est donc avérée nécessaire.
Différents logiciels tels que MySQL et Apache ont dû être configurés pour
obtenir le comportement désiré : servir un programme utilisant une base de 
données via une interface web.  
Des mots de passe aléatoires et par défaut sont générés au premier démarrage de
l'image avant la configuration des différents services. Cette étape permet à
l'utilisateur de gagner du temps.

![Interface de login NextCloud](./images/nextcloud2.png)

Créer une image de toute pièce s'avère compliqué. L'automatisation système se
retrouve très souvent en défaut pour cause d'un simple oubli. Build une image
est un processus long. Une configuration, pourtant simple, peut ainsi mettre
plusieurs heures à être écrite complètement.

Une fois l'image prête et testée en local, il convient de lancer les builds sur
Jenkins. Ici, les images seront créées pour chaque architecture et les tests
lancés sur chacune d'entre elles.  
Des tests basiques ici incluent la vérification de l'installation de la base
de données ainsi que de ses identifiants et l'accès au site fonctionnel.  
Cette étape passée, il convient d'ajouter les images générées à la marketplace.
Pour cela, celles-ci sont dans un premier temps marquées comme _publiques_ pour
pouvoir être utilisées par les clients, puis ajoutées via un _POST_ sur
l'api-marketplace.

La version actuelle de l'image est ensuite mise à jour et apparaît donc par
défaut sur l'interface client.

\newpage

### Étude sur l'import, export et transfert d'images

\index{image}
\index{snapshot}

Lors de la création d'une image, celle-ci est confinée dans sa région. Il 
n'existe à ce jour pas de moyen automatisé pour transférer ses données d'une
zone à l'autre. Une solution à ce problème de longue date a donc dû être 
proposée.

Deux cas de figure sont ici à étudier :

* en interne, lors de la création ou mise à jour d'une image, celle-ci doit
être répliquée pour pouvoir être utilisée sur toutes les zones.
* un client peut souhaiter utiliser ses snapshots ou images dans d'autres zones

![Première version du worker-sync-images](./images/sync-images.png)

Dans un premier temps, l'utilisation interne est à privilégier. Une étude sur
une architecture possible a donc été faite. Il en a résulté un worker capable
de synchroniser des images et snapshots entre les zones.  
Malheureusement, dû à des contraintes d'expérience et de temps de réalisation,
ce projet n'a pu partir en production avant que le SI ne change. De nouvelles
modifications devant ainsi être apportées, le projet est resté de côté pendant
plusieurs mois.

Le design du worker a cependant été repris par après, son utilité se faisant
sentir davantage qu'auparavant.  
Encore au stade d'étude, un nouveau worker modulaire pourrait donc à terme
permettre ces transferts. La modularité de ce worker rend également possible
l'export et l'import d'images ou snapshots au format qcow2, accompagnés de
leurs métadonnées

![Proposition d'architecture pour les transferts](./images/worker-transfers.png)

Dans cette architecture[^10], le transfert n'est contraint que par la reprise sur
erreur et le stockage des données intermédiaires. L'import, quant à lui, pose
des problèmes de sécurité quant à l'émulateur QEMU. Son développement a donc
été mis de côté en attendant sa repriorisation.

Cette architecture, comparée à la précédente, s'appuie sur davantage de
composants existants, tels que le _worker-si_, en charge de déléguer les
différentes tâches aux autres workers.

[^10]: Schéma également disponible en annexes

\newpage

### Migration de Jenkins à Gitlab-CI

\index{CI-CD}
\index{jenkins}
\index{gitlab}
\index{pull-request}

Dans l'état actuel du système d'information, Scaleway utilise majoritairement
Jenkins pour tester et créer les _builds_ de ses projets. Une équipe en charge
de l'intégration continue (CI-CD\index{CI-CD}) a toutefois récemment mis en place
_Gitlab-CI_, la CI intégrée dans l'interface de Gitlab.  
Gitlab étant utilisé par toutes nos équipes, il est plus aisé d'obtenir les
résultats des tests et autres builds directement. Un intérêt majeur de changer
de système est ici de pouvoir lancer des tests sur toutes les branches, mais
aussi sur les merge-requests. Ceci étant moins évident et intégré avec Jenkins,
il était nécessaire de se reposer sur des tests en local ou la bonne foi du
développeur pour la valider.

Gitlab-CI permet, via une configuration en yaml, de définir les différentes
grandes étapes (stages) et sous-étapes liées à un commit ou tag. Ainsi, pour un
projet python, sont définies les étapes _test_, _build_ et _venv_, permettant
respectivement de lancer les tests du projet, de créer un package (wheel) et le
venv associé. Lors d'un déploiement, le venv sera récupéré par la machine
faisant tourner le programme.

![Pipeline Gitlab-CI pour l'api-marketplace](./images/gitlab-ci.png)

Passer de Jenkins à Gitlab-CI est relativement aisé. Une autre équipe, Billing,
a en effet déjà migré ses projets. J'ai pu, grâce à leur aide et à celle de 
l'équipe CI-CD\index{CI-CD} développer rapidement un _proof-of-concept_ pour vérifier la
faisabilité de la migration sur nos projets.  
Bien que nombreux et longs à traiter, la majorité des problèmes rencontrés
a été facile à gérer :

* problèmes mineurs de configuration -> bien relire la documentation
* identifiants manquants -> contacter les équipes compétentes ou les retrouver
* problème d'architecture -> bien s'imaginer comment communique chaque élément


Le processus de build Jenkins, bien qu'automatisé, reste assez manuel : les
tests sont en effet lancés automatiquement à chaque commit mais le build du
package et la création du venv, sont, elles, des actions manuelles. Des erreurs
peuvent résulter de ces actions s'effectuant en local dans un environnement non
contrôlé. Regrouper toutes ces actions sur Gitlab permet ainsi de mieux 
contrôler ce processus tout en l'accélérant.

\newpage

# Premier bilan

## Intérêt de la mission pour l'entreprise

Dans le cadre de son pivot entamé fin 2017, Scaleway recherchait activement des
développeurs compétents afin de faire croître son activité. La majorité des
projets commencés et continués ont donc une vocation à long terme : développer
de nouveaux produits pour s'insérer dans un marché très compétitif.

Bien que créée il y a 20 ans, Scaleway n'opère que depuis très récemment dans
le domaine du Cloud. La vocation de l'entreprise n'est pas de détrôner les
géants actuels comme Amazon, IBM ou encore Google, mais d'innover et de 
concevoir différemment ses produits.  
Le cloud est un domaine qui bouge beaucoup, on observe en effet chez des
concurrents plus directs comme OVH ou DigitalOcean des positionnements
similaires aux nôtres, et parfois inversement.

Par exemple, le produit GPU de Scaleway se met en concurrence directe avec les
gammes d'OVH ou encore Google.

![Positionnement du produit GPU](./images/gpu2.png)

Ce produit, parmi d'autres, très imbriqué dans l'écosystème compute, est
ainsi dépendant de notre équipe pour certaines de ses fonctionnalités.

Ma mission, en tant que DevOps\index{devops}, est donc de maintenir et développer
l'infrastructure compute, aussi bien pour faire évoluer notre propre produit,
mais aussi pour accompagner les autres équipes qui en dépendent.  
Les différentes tâches accomplies permettent ainsi :

  * Un gain de productivité via l'automatisation
  * De nouvelles fonctionnalités client et admin au niveau des instances\index{instance}
  * Un maintien à jour des produits existants
  * De nouveaux produits via les autres équipes

Les éléments modifiés sur l'infrastructure actuelle ont pour vocation
d'être aisément maintenables et ainsi modifiés pour des utilisations futures.
Le travail réalisé est ainsi directement visible à court terme, car déployé en
production et utilisé au jour le jour. À long terme, certaines des tâches
seront rendues obsolètes avec les divers décalages technologiques dûs au
temps. Certaines, en revanche ont pour vocation de rester telles quelles.  
Une des grandes difficultés dans le domaine de l'informatique est justement de
prévoir ces obsolescences. Il est très difficile de prévoir une architecture
résiliente aux effets du temps.


\newpage

## Intérêt personnel

Ayant travaillé dans un milieu peu technique lors de mon précédent poste, je
pensais avoir un bagage technique conséquent en arrivant à Scaleway. Les
premiers jours ont ainsi été très difficiles, l'ensemble de l'infrastructure
étant en général très technique chez Scaleway.

J'ai ainsi au cours de cet apprentissage énormément gagné en expérience, autant
sur des sujets que je pensais maîtriser, mais aussi sur des technologies qui
m'étaient alors inconnues. Ainsi, j'ai pu développer mes connaissances à l'aide
d'une infrastructure comprenant des milliers de serveurs, requérant une grande
rigueur lors de chaque développement.  

#### Langages

\index{python}

Ces connaissances partent ainsi d'une brique fondamentale : le code. J'ai pu
découvrir des aspects de Python que je n'avais jamais utilisé sur des projets
personnels, tels que les tasks queues comme Celery. Dans le cadre des
différents projets j'ai également dû me former davantage sur les langages Go
et bash\index{bash}.  
D'autres projets comme la console client, habituellement hors de portée de
notre équipe, m'ont permis de renforcer mes connaissances en terme de
développement web.

#### Technologies

La majorité des technologies décrites dans la partie 5.1 m'étaient étrangères
ou peu connues au départ. La compréhension de l'utilisation de ces outils étant
primordiale, il m'a fallu lire de la documentation et me former, soit seul,
soit avec l'aide de mes collègues.

\index{docker}
\index{linux}

Notre utilisation poussée de docker m'a par exemple permis d'apprendre son
fonctionnement, celui des services associés mais surtout certains principes de
base de la conteneurisation Linux.  
L'avantage de l'équipe _Compute_ au sein de Scaleway, est de travailler sur un
panel très large de technologies : j'ai ainsi aussi bien pu travailler sur
des kernels et des initrd que sur du développement web ou d'API, deux mondes
opposés.  
Ce même concept d'API ne m'était pas familier au départ, je peux maintenant
prétendre le comprendre et saurais l'implémenter pour mes prochains projets
personnels.

#### Système

\index{linux}

Nos missions, concernant aussi bien du code que de l'administration système par
moments, requiert de nombreuses compétences dans ce dernier domaine. Le grand 
nombre de logiciels, distributions Linux et architectures rendent ce travail
complexe et formateur. Chaque spécificité doit être connue, retenue et prise
en compte.


#### Organisationnel

C'est en entrant chez Scaleway que j'ai pu découvrir la méthode Agile, ainsi
que la communication aussi bien interne qu'entre les différentes équipes. Sans
expérience, il n'est en effet pas simple de se coordonner, de s'exprimer
clairement et rapidement.  
C'est seulement au bout de plusieurs mois, de différents tests et
d'expérimentations  que l'on vient à comprendre comment fonctionne chaque
équipe et que l'on trouve le meilleur moyen d'effectuer ce que l'on souhaite.


Ainsi, les différents aspects organisationnels et techniques acquis durant cet
apprentissage m'ont permis de mieux m'organiser. J'ai, par exemple, mis en
place un programme de suivi de projet, Youtrack, similaire à Jira\index{Jira}. Celui-ci me
permet d'organiser avec aisance mes différents projets, avec des rappels, des
pièces jointes, des commentaires, de la documentation.

![Gestion personnelle des tâches](./images/youtrack.png)


\newpage
## Conclusion et retour d'expérience sur la mission

D'un point de vue général, cette mission m'a été extrêmement bénéfique. Que ce
soit sur le plan relationnel ou technique, il est évident que j'ai pu apprendre
beaucoup en évoluant chez Scaleway.

Chaotique au départ, mon apprentissage s'est ensuite amélioré au fil des mois,
au fur et à mesure que j'assimilais les tenants et aboutissants des différents
projets. Le fait que Scaleway se réorganise à mon arrivée n'a également pas
aidé : l'onboarding était en effet bien trop succinct.  
C'est pourquoi, avec l'aide de mes collègues, j'ai pu travailler à
l'amélioration de nos processus et documentations, afin d'aider toute personne
amenée à travailler ou prendre connaissance de nos projets.

Scaleway évoluant dans un contexte concurrentiel important, il est parfois
nécessaire de faire des raccourcis sur certains projets. J'aurais ainsi aimé
pouvoir passer plus de temps sur des projets importants mais jugés non
prioritaires, comme la synchronisation des images entre les différentes AZ
\index{availability zone}, l'import et l'export de qcow\index{qcow}, ou encore
le pipeline de création et de mise à jour d'images.

La formation d'EPITA\index{EPITA} a été plutôt satisfaisante quant à mes attentes
professionnelles et personnelles. De nombreux cours permettent de s'épanouir en
étendant sa culture technique, juridique et managériale.  
Je regrette toutefois, ayant suivi un cursus d'ingénieur, de ne pas avoir
sensiblement plus de cours axés sur la technique en dernière année. J'aurais
en effet pu bénéficier d'une plus grande facilité au sein de mon entreprise si
j'avais pu avoir des cours devops\index{devops}. Je reconnais toutefois que la majorité des
étudiants n'évoluent pas dans des entreprises ayant de tels besoins.

\newpage

\addcontentsline{toc}{section}{Table des illustrations}
\listoffigures


\newpage
\printglossary[]
\addcontentsline{toc}{section}{Glossaire}

\newpage
\addcontentsline{toc}{section}{Index}
\printindex

\newpage
\begin{thebibliography}{9}
\addcontentsline{toc}{section}{Bibliographie}

\bibitem{}
  Stack Overflow.
  Where Developers Learn, Share, \& Build Careers.

  [En ligne] Disponible sur : https://stackoverflow.com/. 
  2019.

\bibitem{}
  Stein, C., Cormen, T., Rivest, R. and Leiserson, C.
  2002. 
  \emph{Introduction à l'algorithmique}.
  2nd ed. France: Dunod.

\bibitem{}
  Celery - Distributed Task Queue.
  Celery 4.3.0 documentation. 

  [En ligne] disponible sur : http://docs.celeryproject.org/.
  2019.

\bibitem{}
  Scaleway.
  Scaleway Developers Website.

  [En ligne] Disponible sur : https://developers.scaleway.com/.
  2019.


\bibitem{}
  Docker.
  Docker Documentation. 

  [En ligne] Disponible sur : https://docs.docker.com/.
  2019.

\bibitem{}
  Gitlab. 
  GitLab Documentation. 
  
  [En ligne] Disponible sur : https://docs.gitlab.com/.
  2019. 

\bibitem{}
  Wikipédia.
  Wikipédia, l'encyclopédie libre. 
  
  [En ligne] Disponible sur : https://fr.wikipedia.org.
  2019. 

\end{thebibliography}

\vspace{6mm}

L'utilisation des nombreux outils présents chez Scaleway peut s'avérer
difficile au premier abord. Les documentations de ces différents outils doit
donc être consultée pour être complétée ensuite par des exemples.  
Ces exemples peuvent souvent être retrouvés sur _Stack Overflow_.

Pour avoir une vue d'ensemble et se renseigner sur des technologies, _Wikipédia_
fait office de référence. Si les articles peuvent parfois être sommaires, ils
renvoient généralement vers des sources plus conséquentes si cela devient
nécessaire.

Du fait des besoins de performance et de productivité, j'ai dû consulter des
ouvrages ayant trait aux algorithmes ou encore bonnes pratiques.

\label{End}

\newglossaryentry{DC4}{name=DC4, 
  description={4e datacenter de Scaleway, hébergé dans un abri anti-atomique dans Paris 15e}
}
\newglossaryentry{api-marketplace}{name=api-marketplace, 
  description={API vitrine de Scaleway affichant aux clients les images actuellement disponibles}
}
\newglossaryentry{instance}{name=instance, 
  description={Serveur d'un service cloud, souvent facturé à la minute. Une API est généralement présente pour les piloter}
}
\newglossaryentry{stack technique}{name=stack technique, 
  description={Ensemble de produits de l'entreprise se combinant pour former un tout}
}
\newglossaryentry{devops}{name=devops,
  description={Mouvement en ingénierie informatique combinant développement et administration système}
}
\newglossaryentry{availability zone}{name=availability zone,
  description={Région Scaleway au sein de laquelle les ressources sont confinées}
}
\newglossaryentry{qcow}{name=qcow,
  description={Format d'image utilisé par QEMU}
}
\newglossaryentry{qemu}{name=qemu,
  description={Émulateur de matériel utilisé chez Scaleway, tournant sur les hyperviseurs}
}
\newglossaryentry{pull request}{name=pull-request,
  description={Demande d'ajout de code d'une branche sur une autre via GIT. Aussi appelé Merge-Request}
}
\newglossaryentry{ci cd}{name=ci-cd,
  description={Ensemble de logiciels permettant l'intégration et le développement continu}
}
\newglossaryentry{snapshot}{name=snapshot,
  description={Sauvegarde d'un volume Scaleway, au format qcow}
}
\newglossaryentry{B2B}{name=B2B,
  description={Business to Business, désigne les activités commerciales entre
  entreprises.}
}
\newglossaryentry{cold storage}{name=cold storage,
  description={Stockage « à froid » des données. Un coût financier est requis
  pour chaque opération. Cette technologie vise ainsi le stockage longue
  durée.} 
}
\newglossaryentry{bootscript}{name=bootscript,
  description={Ensemble d'un kernel et initrd, utilisé pour le boot réseau chez
    Scaleway.}
}
\newglossaryentry{worker-local-storage}{name=worker-local-storage,
  description={Worker chargé de la gestion des volumes sur les hyperviseurs.}
}
\newglossaryentry{worker-si}{name=worker-si,
  description={Worker central, délègue les tâches aux autres tout en gérant les
  accès à la base de données.}
}
\newglossaryentry{api-compute}{name=api-compute,
  description={API centrale des instances, gère entre autres les créations de
  serveurs, de volumes, de snapshots ou d'images.}
}

\glsadd{DC4}
\glsadd{api-marketplace}
\glsadd{instance}
\glsadd{stack technique}
\glsadd{devops}
\glsadd{instance}
\glsadd{availability zone}
\glsadd{qcow}
\glsadd{qemu}
\glsadd{pull request}
\glsadd{snapshot}
\glsadd{ci cd}
\glsadd{B2B}
\glsadd{cold storage}
\glsadd{worker-local-storage}
\glsadd{worker-si}
\glsadd{api-compute}
